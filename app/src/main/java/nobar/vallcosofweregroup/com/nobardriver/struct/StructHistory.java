package nobar.vallcosofweregroup.com.nobardriver.struct;

/**
 * Created by Javad Vatan on 9/11/2017.
 */

public class StructHistory {
    private String originAddress, destiantionAddre, order, date, status;

    public StructHistory() {
    }

    public StructHistory(String originAddress, String destiantionAddre, String order, String date, String status) {
        this.originAddress = originAddress;
        this.destiantionAddre = destiantionAddre;
        this.order = order;
        this.date = date;
        this.status = status;
    }

    public String getOriginAddress() {
        return originAddress;
    }

    public void setOriginAddress(String originAddress) {
        this.originAddress = originAddress;
    }

    public String getDestiantionAddre() {
        return destiantionAddre;
    }

    public void setDestiantionAddre(String destiantionAddre) {
        this.destiantionAddre = destiantionAddre;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
