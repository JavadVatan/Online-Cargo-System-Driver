package nobar.vallcosofweregroup.com.nobardriver.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.AppPreferences;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;
import nobar.vallcosofweregroup.com.nobardriver.network.NetworkEventListener;
import nobar.vallcosofweregroup.com.nobardriver.network.OkHttpHandler;
import nobar.vallcosofweregroup.com.nobardriver.util.Util;


public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_CODE_PICK_IMAGE_FROM_GALLEY = 101;
    private static final int REQUEST_CODE_PICK_IMAGE_FROM_CAMERA = 201;
    private String userPhotoPath = null;

    private EditText etName, etFamily, etEmail;
    private CircleImageView ivAvatar;
    private ImageView ivSave;
    private TextView tvRealName, tvSave;
    private AppPreferences preferences;
    private ProgressBar pbSave;
    private TextView tvPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        initVariable();
        initHeader();
        setTextVariable();
    }

    private void initVariable() {
        GlobalFunction globalFunction = GlobalFunction.getInstance();
        globalFunction.overrideFonts(this, getWindow().getDecorView().getRootView());
        globalFunction.changeStatusBarColor(this, R.color.colorPrimaryDarkInWhitePage);

        preferences = AppPreferences.getInstance();
        ivSave = findViewById(R.id.activity_profile_iv_save);
        pbSave = findViewById(R.id.activity_profile_pb_save);
        ivAvatar = findViewById(R.id.activity_profile_iv_avatar);
        etEmail = findViewById(R.id.activity_profile_et_email);
        etName = findViewById(R.id.activity_profile_et_name);
        etFamily = findViewById(R.id.activity_profile_et_family);
        tvPhoneNumber = findViewById(R.id.activity_profile_tv_phone_number);

       /* etName.setText(preferences.getUserName());
        etFamily.setText(preferences.getUserFamily());*/
        ivSave.setOnClickListener(this);
    }

    private void initHeader() {
        int[] headerId = new int[]{R.id.activity_profile_back, R.id.activity_profile_iv_camera,
                R.id.activity_profile_iv_gallery};
        for (int aHeaderId : headerId) {
            findViewById(aHeaderId).setOnClickListener(this);
        }
    }

    private void setTextVariable() {
        tvPhoneNumber.setText(preferences.getUserPhoneNumber());
        etName.setText(preferences.getUserName());
        etFamily.setText(preferences.getUserFamily());
        etEmail.setText(preferences.getUserEmail());

        if (!preferences.getUserPhotoUrl().equals("")) {
            Picasso.with(this).load(preferences.getUserPhotoUrl())
                    .placeholder(R.drawable.ic_avatar)
                    .into(ivAvatar);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_profile_iv_gallery:
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media
                        .EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_CODE_PICK_IMAGE_FROM_GALLEY);
                break;
            case R.id.activity_profile_back:
                onBackPressed();
                break;
            case R.id.activity_profile_iv_camera:
                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE),
                        REQUEST_CODE_PICK_IMAGE_FROM_CAMERA);
                break;
            case R.id.activity_profile_iv_save:
                manageSave();

                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_PICK_IMAGE_FROM_GALLEY && data != null) {
                manageGalleryResult(data);

            } else if (requestCode == REQUEST_CODE_PICK_IMAGE_FROM_CAMERA && data != null) {
                manageCameraResult(data);
            }
        }
    }

    private void manageGalleryResult(Intent data) {
        Uri pickedImage = data.getData();
        String[] filePath = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
        cursor.moveToFirst();
        String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            Util util = new Util();
            ivAvatar.setImageBitmap(util.modifyOrientation(BitmapFactory.decodeFile(imagePath), imagePath));
            userPhotoPath = imagePath;

        } catch (IOException e) {
            e.printStackTrace();
        }

        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        cursor.close();
    }

    private void manageCameraResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        userPhotoPath = destination.getAbsolutePath();
        ivAvatar.setImageBitmap(thumbnail);
    }


    private void manageSave() {
        if (userPhotoPath != null)
            callApiUploadFile();

        callApiSendProfile();
    }

    private void callApiUploadFile() {

        ivSave.setVisibility(View.INVISIBLE);
        pbSave.setVisibility(View.VISIBLE);

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {

                 /*   pbSave.setVisibility(View.INVISIBLE);
                    ivSave.setVisibility(View.VISIBLE);*/
                    GlobalFunction.getInstance().toast(ProfileActivity.this,
                            object.getString(Constants.JSON_MESSAGE));
                    preferences.setUserPhotoUrl(object.getString(Constants.JSON_IMAGE));
                    Picasso.with(ProfileActivity.this)
                            .load(preferences.getUserPhotoUrl())
                            .placeholder(R.drawable.ic_avatar)
                            .into(ivAvatar);

                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
             /*   pbSave.setVisibility(View.INVISIBLE);
                ivSave.setVisibility(View.VISIBLE);*/
                GlobalFunction.getInstance().toast(ProfileActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_UPLOAD_PROFILE, eventListener)
                .addImage(reduceSize(), Constants.JSON_USER_ID_LOWER_CASE, preferences.getUserId())
                .send();
    }

    public File reduceSize() {
        File file = new File(userPhotoPath);
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 50;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    private void callApiSendProfile() {
        final String name = etName.getText().toString().trim();
        final String family = etFamily.getText().toString().trim();
        final String email = etEmail.getText().toString().trim();

        ivSave.setVisibility(View.INVISIBLE);
        pbSave.setVisibility(View.VISIBLE);

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {

                    pbSave.setVisibility(View.INVISIBLE);
                    ivSave.setVisibility(View.VISIBLE);
                    preferences.setUserName(name);
                    preferences.setUserFamily(family);
                    preferences.setUserEmail(email);
                    GlobalFunction.getInstance().toast(ProfileActivity.this,
                            object.getString(Constants.JSON_MESSAGE));

                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                pbSave.setVisibility(View.INVISIBLE);
                ivSave.setVisibility(View.VISIBLE);
                GlobalFunction.getInstance().toast(ProfileActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_SEND_PROFILE, eventListener)
                .addParam(Constants.JSON_MOBILE,/* AppPreferences.getInstance().getUserPhoneNumber()*/"09150747687")
                .addParam(Constants.JSON_NAME, name)
                .addParam(Constants.JSON_FAMILY, family)
                .addParam(Constants.JSON_EMAIL, email)
                // .addParam(Constants.JSON_IMAGE, preferences.getUserPhotoUrl())
                .send();
    }


}
