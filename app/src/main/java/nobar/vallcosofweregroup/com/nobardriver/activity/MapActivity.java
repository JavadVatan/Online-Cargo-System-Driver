package nobar.vallcosofweregroup.com.nobardriver.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {
    public static final String TAG_MAP_LAT = "map_latitude";
    public static final String TAG_MAP_LNG = "map_longitude";
    private static final int REQUEST_CODE_PERMISSION_LOCATION = 1;
    private MapView mapView;
    private GoogleMap mGoogleMap;
    private LatLng destinationLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initBundle();
        if (checkPermission())
            initMap(savedInstanceState);
    }

    private void initBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            double lat = bundle.getDouble(TAG_MAP_LAT);
            double lng = bundle.getDouble(TAG_MAP_LNG);
            destinationLatLng = new LatLng(lat, lng);
        }
    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_CODE_PERMISSION_LOCATION);
            return false;
        }
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (checkPermission()) {
            mGoogleMap = googleMap;
            mGoogleMap.setMyLocationEnabled(true);
            addDestinationMarker();
        }
    }

    private void initMap(Bundle savedInstanceState) {
        mapView = findViewById(R.id.frg_maps_map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);
    }

    private void addDestinationMarker() {
        if (destinationLatLng == null) {
            GlobalFunction.getInstance().toast(this, getString(R.string.location_is_not_available));
            return;
        }
        mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(destinationLatLng.latitude, destinationLatLng.longitude))
                .title(getString(R.string.destination))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_destination)));
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(destinationLatLng,
                15f));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initMap(null);
                } else {
                    finish();
                }
                break;

        }
    }
}
