package nobar.vallcosofweregroup.com.nobardriver.main.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.activity.FactorActivity;
import nobar.vallcosofweregroup.com.nobardriver.adapter.OrderAdapter;
import nobar.vallcosofweregroup.com.nobardriver.core.BaseFragment;
import nobar.vallcosofweregroup.com.nobardriver.global.AppPreferences;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;
import nobar.vallcosofweregroup.com.nobardriver.global.TemporaryPreferences;
import nobar.vallcosofweregroup.com.nobardriver.network.NetworkEventListener;
import nobar.vallcosofweregroup.com.nobardriver.network.OkHttpHandler;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructData;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructExpensiveTools;

public class HomeFragment extends BaseFragment {
    private RecyclerView mRecyclerView;
    private OrderAdapter mAdapter;
    private List<StructData> dataList = new ArrayList<>();

    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        currView = inflater.inflate(R.layout.frg_home, container, false);

        initialize();

        return currView;
    }

    private void initialize() {
        GlobalFunction.getInstance().overrideFonts(getContext(), currView);
        initVariable();
        initList();
        callApiGetOrder();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getContext() != null)
            callApiGetOrder();
    }

    private void initVariable() {
        mRecyclerView = currView.findViewById(R.id.frg_home_recycler);
    }

    private void initList() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new OrderAdapter(dataList) {
            @Override
            public void onOrderClicked(int position) {
                if (manageDeactivateDriver()) {
                    TemporaryPreferences.getInstance().addPrefData(dataList.get(position));
                    manageFactor(FactorActivity.TAG_MODE_NORMAL);
                }
            }
        };
        mRecyclerView.setAdapter(mAdapter);
    }

    private boolean manageDeactivateDriver() {
        boolean status = AppPreferences.getInstance().isActiveDriver();
        if (!status)
            GlobalFunction.getInstance().toast(getContext(), getString(R.string.error_user_in_active));
        return status;
    }

    private void manageFactor(int type) {
        Intent intent = new Intent(getContext(), FactorActivity.class);
        intent.putExtra(FactorActivity.TAG_MODE, type);
        startActivity(intent);
    }

    private void callApiGetOrder() {
        String link = Constants.NETWORK_ACTION_GET_ORDER + "/" + AppPreferences.getInstance().getUserId();

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    parseJson(object);
                    mAdapter.notifyDataSetChanged();
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                GlobalFunction.getInstance().toast(getContext(), string);
            }
        };

        new OkHttpHandler(getContext(), link, eventListener)
                .send();
    }

    private void parseJson(JSONObject jobject) throws JSONException {
        dataList.clear();
        JSONArray objectArray = jobject.getJSONArray(Constants.JSON_DATA);
        for (int i = 0; i < objectArray.length(); i++) {
            JSONObject object = objectArray.getJSONObject(i);
            StructData data = new StructData();

            data.setBarbar_worker(object.getString(Constants.JSON_BARBER_WORKER));
            data.setCar_id(object.getString(Constants.JSON_CAR_ID));
            data.setChideman_worker(object.getString(Constants.JSON_CHIDEMAN_WORKER));
            data.setDestination_floor(object.getString(Constants.JSON_DESTINATION_FLOOR));
            data.setDate(object.getString(Constants.JSON_TIME));
            data.setDestination_walking(object.getString(Constants.JSON_DESTINATION_WALKING));
            data.setId(object.getString(Constants.JSON_ID));
            data.setOrigin_floor(object.getString(Constants.JSON_ORIGIN_FLOOR));
            data.setOrigin_walking(object.getString(Constants.JSON_ORIGIN_WALIKING));
            data.setPay_with_reciver(object.getString(Constants.JSON_PAY_WITH_RECEIVER));
            data.setPrice(object.getString(Constants.JSON_PRICE));
            data.setReciver_mobile(object.getString(Constants.JSON_RECEIVER_MOBILE));
            data.setReciver_name(object.getString(Constants.JSON_RECEIVER_NAME));
            data.setStatus(object.getString(Constants.JSON_STATUS));
            data.setUser_id(object.getString(Constants.JSON_USER_ID));

            //parse Address
            data.setStartAddressTitle(object.getJSONObject("0").getString(Constants.JSON_START_ADDRESS_TITLE));
            data.setStartAddressLat(object.getJSONObject("0").getString(Constants.JSON_START_ADDRESS_LAT));
            data.setStartAddressLng(object.getJSONObject("0").getString(Constants.JSON_START_ADDRESS_LNG));
            data.setEndAddressTitle(object.getJSONObject("0").getString(Constants.JSON_END_ADDRESS_TITLE));
            data.setEndAddressLat(object.getJSONObject("0").getString(Constants.JSON_END_ADDRESS_LAT));
            data.setEndAddressLng(object.getJSONObject("0").getString(Constants.JSON_END_ADDRESS_LNG));


            parseJsonExpensiveTools(object, data);
            dataList.add(data);
        }

    }

    private void parseJsonExpensiveTools(JSONObject mainObject, StructData structData) throws JSONException {

        if (!mainObject.getString(Constants.JSON_VASAYELE_HAZINEHDAR).equals("")) {
            JSONObject jsonObject = new JSONObject(mainObject.getString(Constants.JSON_VASAYELE_HAZINEHDAR));
            JSONArray arrayExpensive = jsonObject.getJSONArray(Constants.JSON_DATA);

            for (int i = 0; i < arrayExpensive.length(); i++) {
                JSONObject objectExpensive = arrayExpensive.getJSONObject(i);
                StructExpensiveTools struct = new StructExpensiveTools();
                struct.setId(objectExpensive.getString(Constants.JSON_ID));
                struct.setCount(objectExpensive.getString(Constants.JSON_COUNT));
                if (objectExpensive.has(Constants.JSON_SUBJECT))
                    struct.setTitle(objectExpensive.getString(Constants.JSON_SUBJECT));
                structData.getVasayeleHazinedar().add(struct);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUserVisibleHint(true);
    }
}
