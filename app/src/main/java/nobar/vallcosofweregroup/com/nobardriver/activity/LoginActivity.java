package nobar.vallcosofweregroup.com.nobardriver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.AppPreferences;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;
import nobar.vallcosofweregroup.com.nobardriver.network.NetworkEventListener;
import nobar.vallcosofweregroup.com.nobardriver.network.OkHttpHandler;

public class LoginActivity extends AppCompatActivity {

    private EditText etPhoneNumber, etPassword;
    private ImageView ivLogin;
    private ProgressBar pbLogin;
    private AppPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //actionbar(0, 0);
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        preferences = AppPreferences.getInstance();
        pbLogin = findViewById(R.id.activity_login_pb_confirm);
        ivLogin = findViewById(R.id.activity_login_iv_confirm);
        etPhoneNumber = findViewById(R.id.activity_login_et_phone_number);
        etPassword = findViewById(R.id.activity_login_et_password);


        ivLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation())
                    login(etPhoneNumber.getText().toString().trim(), etPassword.getText().toString().trim());
            }
        });
    }

    private boolean validation() {
   /*     if (etPhoneNumber.getText().toString().length() == 0) {
            etPhoneNumber.setError(getString(R.string.error_empty_phone_number));
            return false;
        } else if (etPhoneNumber.getText().toString().length() != 11) {
                etPhoneNumber.setError(getString(R.string.error_uncorrect_phone_number));
            return false;
        }
        if (etPassword.getText().toString().length() == 0) {
            etPassword.setError(getString(R.string.error_empty_password));
            return false;
        }*/
        return true;
    }

    private void login(final String phone, final String password) {
        ivLogin.setVisibility(View.INVISIBLE);
        pbLogin.setVisibility(View.VISIBLE);
        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200) ||
                        object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_402)) {

                    pbLogin.setVisibility(View.INVISIBLE);
                    ivLogin.setVisibility(View.VISIBLE);

                    preferences.setUserPhoneNumber(phone);
                    preferences.setDriverId(object.getJSONArray(Constants.JSON_DRIVER_ID)
                            .getJSONObject(0).getString(Constants.JSON_ID));
                    preferences.setUserLogin(true);

                    GlobalFunction.getInstance().toast(LoginActivity.this,
                            object.getString(Constants.JSON_MESSAGE));

                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();

                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                pbLogin.setVisibility(View.INVISIBLE);
                ivLogin.setVisibility(View.VISIBLE);
                GlobalFunction.getInstance().toast(LoginActivity.this, string);
            }

        };

        new OkHttpHandler(this, Constants.NETWORK_ACTION_LOGIN, eventListener)
                .addParam(Constants.JSON_USER_NAME, phone)
                .addParam(Constants.JSON_PASSWORD, password)
                .send();

    }


}
