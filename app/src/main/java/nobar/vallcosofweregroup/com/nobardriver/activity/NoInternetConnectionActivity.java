package nobar.vallcosofweregroup.com.nobardriver.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.Application;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;


public class NoInternetConnectionActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvTryAgain, tvCheckNetwork;
    private GlobalFunction globalFunction = GlobalFunction.getInstance();
    private Uri currUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet_connection);
        initBundle();
        initVariable();
    }

    private void initBundle() {
        if (getIntent() != null)
            currUri = getIntent().getData();

    }

    private void initVariable() {
        tvCheckNetwork = findViewById(R.id.internet_connection_tv_check_network_setting);
        tvTryAgain = findViewById(R.id.no_internet_connection_tv_try_again);
        ((TextView) findViewById(R.id.no_internet_connection_tv_error)).setTypeface(Constants.iranSenseBold);

        tvCheckNetwork.setTypeface(Constants.iranSenseLight);
        tvTryAgain.setTypeface(Constants.iranSenseLight);
        tvTryAgain.setOnClickListener(this);
        tvCheckNetwork.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.internet_connection_tv_check_network_setting:
                globalFunction.openNetworkSetting(this);
                break;
            case R.id.no_internet_connection_tv_try_again:
                if (Application.getInstance().isConnectionAnyInternet()) {
                    manageTryAgain();
                }

                break;
        }
    }

    private void manageTryAgain() {
        if (currUri != null) {
            Intent i = new Intent(Intent.ACTION_VIEW, currUri);
            startActivity(i);
        } else {
            onBackPressed();
        }
        finish();
    }
}
