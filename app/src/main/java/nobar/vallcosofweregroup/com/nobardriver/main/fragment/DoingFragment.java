package nobar.vallcosofweregroup.com.nobardriver.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.core.BaseFragment;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;

public class DoingFragment extends BaseFragment {

    public static DoingFragment getInstance() {
        return new DoingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        currView = inflater.inflate(R.layout.frg_doing, container, false);

        initialize();

        return currView;
    }

    private void initialize() {
        GlobalFunction.getInstance().overrideFonts(getContext(), currView);

    }

}
