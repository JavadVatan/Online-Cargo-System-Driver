package nobar.vallcosofweregroup.com.nobardriver.network;

import org.json.JSONException;
import org.json.JSONObject;

public interface NetworkEventListener {


    void onSuccess(JSONObject object) throws JSONException;


    void onError(String string);
}
