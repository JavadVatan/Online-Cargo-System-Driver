package nobar.vallcosofweregroup.com.nobardriver.struct;

/**
 * Created by Javad Vatan on 11/28/2017.
 */

public class StructExpensiveTools {
    private String id, count, title;

    public StructExpensiveTools(String id, String count, String title) {
        this.id = id;
        this.count = count;
    }

    public StructExpensiveTools() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
