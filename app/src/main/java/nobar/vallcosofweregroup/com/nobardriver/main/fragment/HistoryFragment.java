package nobar.vallcosofweregroup.com.nobardriver.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.adapter.HistoryAdapter;
import nobar.vallcosofweregroup.com.nobardriver.core.BaseFragment;
import nobar.vallcosofweregroup.com.nobardriver.global.AppPreferences;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;
import nobar.vallcosofweregroup.com.nobardriver.network.NetworkEventListener;
import nobar.vallcosofweregroup.com.nobardriver.network.OkHttpHandler;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructHistory;

public class HistoryFragment extends BaseFragment {
    private RecyclerView mRecyclerView;
    private HistoryAdapter mAdapter;
    private List<StructHistory> dataList = new ArrayList<>();

    public static HistoryFragment getInstance() {
        return new HistoryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        currView = inflater.inflate(R.layout.frg_history, container, false);

        initialize();

        return currView;
    }

    private void initialize() {
        GlobalFunction.getInstance().overrideFonts(getContext(), currView);
        initVariable();
        initList();
        callApiGetHistory();
    }


    private void initVariable() {
        mRecyclerView = currView.findViewById(R.id.frg_history_recycler);
    }

    private void initList() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new HistoryAdapter(dataList);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void callApiGetHistory() {

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    JSONArray objectArray = object.getJSONArray(Constants.JSON_DATA);

                    for (int i = 0; i < objectArray.length(); i++) {
                        JSONObject jsonObject = objectArray.getJSONObject(i);
                        if (!jsonObject.toString().equals("{}")) {
                            JSONObject realObject = jsonObject.getJSONObject("0");

                            dataList.add(new StructHistory(
                                    realObject.getString("startAddress"),
                                    realObject.getString("endAddress"),
                                    realObject.getString(Constants.JSON_ID),
                                    realObject.getString(Constants.JSON_DATE),
                                    manageStatus(Integer.parseInt(realObject.getString(Constants.JSON_STATUS)))));
                        }
                    }


                    mAdapter.notifyDataSetChanged();
                } else
                    onError(object.getString(Constants.JSON_MESSAGE));
            }

            public void onError(String string) {
                if (string != null && string.length() > 0 && !string.equals(getString(R.string.spam_message_from_server)))
                    GlobalFunction.getInstance().toast(getContext(), string);
            }

        };

        new OkHttpHandler(getContext(), Constants.NETWORK_TRANSPORTATION_HISTORY, eventListener)
                .addParam(Constants.JSON_DRIVER_ID, AppPreferences.getInstance().getUserId())
                .send();
    }

    private String manageStatus(int status) {
        switch (status) {
            case 1:
                return "وضعیت خطای سیستمی ";

            case 2:
                return "در انتظار رسیدن به مقصد ";

            case 3:
                return "در انتظار شروع فرایند بارگیری ";

            case 4:
                return "در حال بارگیری ";

            case 5:
                return "پایان بارگیری ";

            case 6:
                return "سفر توسط شما لغو شده است ";

            case Constants.STATUS_DRIVER_FINISHED_ORDER:
                return "سفر به اتمام رسیده";

            default:
                return "وضعیت نامشخص ";
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getContext() != null)
            callApiGetHistory();

    }
}
