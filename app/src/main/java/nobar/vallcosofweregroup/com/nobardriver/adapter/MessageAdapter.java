package nobar.vallcosofweregroup.com.nobardriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.dialog.ConfirmationDialog;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructMessage;


/**
 * Created by Javad on 8/5/2016.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    private List<StructMessage> dataList;
    private Context mContext;

    public MessageAdapter(List<StructMessage> dataList) {
        this.dataList = dataList;
    }

    @Override
    public MessageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MessageAdapter.ViewHolder holder, int position) {
        holder.tvName.setText(dataList.get(position).subject);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvDetail;
        private ImageView imgIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.item_message_tv);
            imgIcon = itemView.findViewById(R.id.item_message_iv);
            tvName.setTypeface(Constants.iranSenseLight);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialog();
                }
            });

        }

        private void showDialog() {
            ConfirmationDialog.DialogConfirmationHandler handler = new ConfirmationDialog
                    .DialogConfirmationHandler() {
                @Override
                public void onPositiveDialogConfirmationClicked() {
                }

                @Override
                public void onNegativeDialogConfirmationClicked() {

                }

                @Override
                public void onMoreDialogConfirmationClicked() {

                }
            };

            ConfirmationDialog dialogConfirmation = new ConfirmationDialog(mContext)
                    .setDataDialog(handler, dataList.get(getAdapterPosition()).subject,
                            dataList.get(getAdapterPosition()).subject,
                            null,
                            mContext.getString(R.string.confirm));

            dialogConfirmation.setCanceledOnTouchOutside(false);
            dialogConfirmation.onCreateDialog();
        }
    }
}
