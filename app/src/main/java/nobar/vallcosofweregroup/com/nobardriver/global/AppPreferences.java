package nobar.vallcosofweregroup.com.nobardriver.global;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Javad Vatan on 9/5/2017.
 */

public class AppPreferences {


    private static AppPreferences myInstance;
    private Context mContext;
    private SharedPreferences mSharedPreferences;

    public AppPreferences() {
        this.mContext = Application.getInstance().getContext();
        mSharedPreferences = mContext.getSharedPreferences(
                "com.nobaar.android.global", Context.MODE_PRIVATE);
    }

    public static AppPreferences getInstance() {
        if (myInstance == null) {
            myInstance = new AppPreferences();
        }
        return myInstance;
    }

    public boolean getUserLogin() {
        return mSharedPreferences.getBoolean(Constants.Preferences_USER_LOGIN, false);
    }

    public void setUserLogin(boolean status) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(Constants.Preferences_USER_LOGIN, status);
        editor.apply();
    }

    public String getUserName() {
        return mSharedPreferences.getString(Constants.Preferences_USER_NAME, "");
    }

    public void setUserName(String fullName) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.Preferences_USER_NAME, fullName);
        editor.apply();
    }

    public String getUserFamily() {
        return mSharedPreferences.getString(Constants.Preferences_USER_FAMILY, "");
    }

    public void setUserFamily(String fullName) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.Preferences_USER_FAMILY, fullName);
        editor.apply();
    }

    public String getUserId() {
        return mSharedPreferences.getString(Constants.Preferences_USER_ID, "-1");
    }

    public void setDriverId(String id) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.Preferences_USER_ID, id);
        editor.apply();
    }

    public String getUserEmail() {
        return mSharedPreferences.getString(Constants.Preferences_USER_EMAIL, "");
    }

    public void setUserEmail(String email) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.Preferences_USER_EMAIL, email);
        editor.apply();
    }

    public String getIsFirst() {
        return mSharedPreferences.getString(Constants.Preferences_IS_FIRST, "0");
    }

    public void setIsFirst(String state) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.Preferences_IS_FIRST, state);
        editor.apply();
    }


    public String getUserPhoneNumber() {
        return mSharedPreferences.getString(Constants.Preferences_USER_PHONE, "-1");
    }

    public void setUserPhoneNumber(String phoneNumber) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.Preferences_USER_PHONE, phoneNumber);
        editor.apply();
    }

    public String getUserCost() {
        return mSharedPreferences.getString(Constants.Preferences_USER_COST, "0");
    }

    public void setUserCost(String userCost) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.Preferences_USER_COST, userCost);
        editor.apply();
    }


    public String getUserPhotoUrl() {
        return mSharedPreferences.getString(Constants.Preferences_USER_PHOTO_URL, "");
    }

    public void setUserPhotoUrl(String photoUrl) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.Preferences_USER_PHOTO_URL, photoUrl);
        editor.apply();
    }

    public boolean isActiveDriver() {
        return mSharedPreferences.getBoolean(Constants.Preferences_DRIVER_ACTIVE, false);
    }

    public void setActiveDriver(boolean status) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(Constants.Preferences_DRIVER_ACTIVE, status);
        editor.apply();
    }

    public void clearPreferences() {
        mSharedPreferences.edit().clear().apply();
    }


}
