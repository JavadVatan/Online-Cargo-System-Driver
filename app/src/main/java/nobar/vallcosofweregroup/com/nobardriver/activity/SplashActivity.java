package nobar.vallcosofweregroup.com.nobardriver.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.AppPreferences;
import nobar.vallcosofweregroup.com.nobardriver.global.Application;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;


public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH = 2000;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initApplicationVariable();
        manageSplash();
    }

    private void initApplicationVariable() {
        Constants.iranSenseLight = Typeface.createFromAsset(getAssets(), "IRANSans.ttf");
        Constants.iranSenseBold = Typeface.createFromAsset(getAssets(), "IRANSansBold.ttf");
    }

    private void manageSplash() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Application.getInstance().isConnectionAnyInternet()) {
                    Intent mainIntent;
                    if (!AppPreferences.getInstance().getUserId().equals("-1"))
                        mainIntent = new Intent(SplashActivity.this, HomeActivity.class);
                    else
                        mainIntent = new Intent(SplashActivity.this, LoginActivity.class);

                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                } else {
                    Intent mainIntent = new Intent(SplashActivity.this, NoInternetConnectionActivity.class);
                    mainIntent.setData(Uri.parse("nobar_driver://home_activity"));
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                }
            }

        }, SPLASH_DISPLAY_LENGTH);
    }
}
