package nobar.vallcosofweregroup.com.nobardriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructExpensiveTools;

/**
 * Created by Javad on 8/5/2016.
 */
public class ExpensiveToolAdapter extends RecyclerView.Adapter<ExpensiveToolAdapter.ViewHolder> {
    private List<StructExpensiveTools> dataList;
    private Context mContext;

    public ExpensiveToolAdapter(List<StructExpensiveTools> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ExpensiveToolAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_expensive_tools, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ExpensiveToolAdapter.ViewHolder holder, int position) {
        holder.tvTilte.setText(dataList.get(position).getTitle());
        holder.tvCounter.setText(dataList.get(position).getCount());

        manageCounter(holder, position);
    }

    private void manageCounter(final ExpensiveToolAdapter.ViewHolder holder, final int position) {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isPlus = false;
                switch (view.getId()) {
                    case R.id.item_expensive_tools_iv_mines:
                        isPlus = false;
                        break;
                    case R.id.item_expensive_tools_iv_plus:
                        isPlus = true;
                        break;
                }
                int counter = Integer.parseInt(dataList.get(position).getCount());

                if (isPlus)
                    counter++;
                else if (counter > 0)
                    counter--;

                dataList.get(position).setCount(counter + "");
                holder.tvCounter.setText(String.valueOf(counter));
            }
        };

        holder.ivMinus.setOnClickListener(onClickListener);
        holder.ivPlus.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTilte, tvCounter;
        private ImageView ivMinus, ivPlus;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTilte = itemView.findViewById(R.id.item_expensive_tools_tv_title);
            tvCounter = itemView.findViewById(R.id.item_expensive_tools_tv_counter);
            ivMinus = itemView.findViewById(R.id.item_expensive_tools_iv_mines);
            ivPlus = itemView.findViewById(R.id.item_expensive_tools_iv_plus);

            tvTilte.setTypeface(Constants.iranSenseLight);
            tvCounter.setTypeface(Constants.iranSenseLight);
        }
    }
}
