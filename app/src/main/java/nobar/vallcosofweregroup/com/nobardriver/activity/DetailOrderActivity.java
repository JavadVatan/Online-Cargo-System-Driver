package nobar.vallcosofweregroup.com.nobardriver.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appyvet.rangebar.RangeBar;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.adapter.ExpensiveToolAdapter;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;
import nobar.vallcosofweregroup.com.nobardriver.global.TemporaryPreferences;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructData;

public class DetailOrderActivity extends AppCompatActivity implements View.OnClickListener {
    private ExpensiveToolAdapter adapterExpensiveTool;
    private RecyclerView rvExpensiveTool;
    private SwitchCompat scCostThings;
    private TextView tvBarbarWorkerCounter, tvChidemanWorkerCounter, tvConfirm, tvFloorOriginCounter,
            tvFloorDestinationCounter;
    private RangeBar rbOriginWalking, rbDestinationWalking;
    private StructData structData;
    private ProgressBar pbConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);
        initializer();
    }

    private void initializer() {
        initVariable();
        initListExpensiveTool();
        manageExpensiveList();
        setOnClick();
        setData();
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        structData = TemporaryPreferences.getInstance().getPrefData();
        scCostThings = findViewById(R.id.activity_complete_order_sc_cost_things);
        tvBarbarWorkerCounter = findViewById(R.id.activity_complete_order_tv_carry_worker_counter);
        tvChidemanWorkerCounter = findViewById(R.id.activity_complete_order_tv_cat_worker_counter);
        tvFloorDestinationCounter = findViewById(R.id.activity_complete_order_tv_floor_destination_counter);
        tvFloorOriginCounter = findViewById(R.id.activity_complete_order_tv_floor_origin_counter);
        tvConfirm = findViewById(R.id.activity_complete_tv_confirm);
        pbConfirm = findViewById(R.id.activity_complete_pb_confirm);
        rbOriginWalking = findViewById(R.id.activity_main_origin_rb_walking);
        rbDestinationWalking = findViewById(R.id.activity_main_destination_rb_walking);
        rvExpensiveTool = findViewById(R.id.activity_complete_order_rv_expensive_tool);

        rbOriginWalking.setTickEnd(100);
        rbDestinationWalking.setTickEnd(100);
        rbOriginWalking.setSeekPinByValue(0);
        rbDestinationWalking.setSeekPinByValue(0);
    }

    private void initListExpensiveTool() {
        rvExpensiveTool.setNestedScrollingEnabled(false);
        rvExpensiveTool.setHasFixedSize(true);
        rvExpensiveTool.setLayoutManager(new LinearLayoutManager
                (this, LinearLayoutManager.VERTICAL, true));

        adapterExpensiveTool = new ExpensiveToolAdapter(structData.getVasayeleHazinedar());
        rvExpensiveTool.setAdapter(adapterExpensiveTool);
    }

    private void manageExpensiveList() {
        if (structData.getVasayeleHazinedar().size() > 0) {
            scCostThings.setChecked(true);
            rvExpensiveTool.setVisibility(View.VISIBLE);
        }

        scCostThings.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                rvExpensiveTool.setVisibility(b ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void setOnClick() {
        ((TextView) findViewById(R.id.include_app_bar_tv_title)).setText(R.string.detail_order);
        findViewById(R.id.include_app_bar_iv_back).setVisibility(View.VISIBLE);
        int[] arraySetClick = new int[]{
                R.id.include_app_bar_iv_back, R.id.activity_complete_order_iv_carry_worker_plus,
                R.id.activity_complete_order_iv_carry_worker_mines, R.id.activity_complete_order_iv_cat_worker_mines,
                R.id.activity_complete_order_iv_cat_worker_plus, R.id.activity_complete_order_iv_floor_origin_plus,
                R.id.activity_complete_order_iv_floor_origin_mines, R.id.activity_complete_order_iv_floor_origin_plus,
                R.id.activity_complete_order_iv_floor_destination_plus, R.id.activity_complete_order_iv_floor_destination_mines
                , R.id.activity_complete_order_rl_cost_things, R.id.activity_complete_tv_confirm};
        for (int i = 0; i < arraySetClick.length; i++) {
            findViewById(arraySetClick[i]).setOnClickListener(this);
        }
    }

    private void setData() {
        tvBarbarWorkerCounter.setText(structData.getBarbar_worker());
        tvChidemanWorkerCounter.setText(structData.getChideman_worker());
        tvFloorDestinationCounter.setText(structData.getDestination_floor());
        tvFloorOriginCounter.setText(structData.getOrigin_floor());
        rbDestinationWalking.setSeekPinByValue(Integer.parseInt(structData.getDestination_walking()));
        rbOriginWalking.setSeekPinByValue(Integer.parseInt(structData.getOrigin_walking()));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.include_app_bar_iv_back:
                onBackPressed();
                break;
            case R.id.activity_complete_tv_confirm:
                //manageConfirmPage();
                break;
            case R.id.activity_complete_order_rl_cost_things:
                scCostThings.setChecked(!scCostThings.isChecked());
                break;
            case R.id.activity_complete_order_iv_cat_worker_plus:
                manageCounter(tvChidemanWorkerCounter, true);
                break;
            case R.id.activity_complete_order_iv_carry_worker_plus:
                manageCounter(tvBarbarWorkerCounter, true);
                break;
            case R.id.activity_complete_order_iv_floor_destination_plus:
                manageCounter(tvFloorOriginCounter, true);
                break;
            case R.id.activity_complete_order_iv_floor_origin_plus:
                manageCounter(tvFloorOriginCounter, true);
                break;
            case R.id.activity_complete_order_iv_cat_worker_mines:
                manageCounter(tvChidemanWorkerCounter, false);
                break;
            case R.id.activity_complete_order_iv_carry_worker_mines:
                manageCounter(tvBarbarWorkerCounter, false);
                break;
            case R.id.activity_complete_order_iv_floor_destination_mines:
                manageCounter(tvFloorOriginCounter, false);
                break;
            case R.id.activity_complete_order_iv_floor_origin_mines:
                manageCounter(tvFloorOriginCounter, false);
                break;

        }
    }

    private void manageCounter(TextView tvCounter, boolean isPlus) {
        int counter;

        counter = Integer.parseInt(tvCounter.getText().toString());

        if (isPlus)
            counter++;
        else if (counter > 0)
            counter--;

        tvCounter.setText(String.valueOf(counter));
    }

}
