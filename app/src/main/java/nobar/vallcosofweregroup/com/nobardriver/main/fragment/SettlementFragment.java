package nobar.vallcosofweregroup.com.nobardriver.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.adapter.SettlementAdapter;
import nobar.vallcosofweregroup.com.nobardriver.core.BaseFragment;
import nobar.vallcosofweregroup.com.nobardriver.global.AppPreferences;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;
import nobar.vallcosofweregroup.com.nobardriver.network.NetworkEventListener;
import nobar.vallcosofweregroup.com.nobardriver.network.OkHttpHandler;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructSettlement;

public class SettlementFragment extends BaseFragment {

    private RecyclerView mRecyclerView;
    private SettlementAdapter mAdapter;
    private List<StructSettlement> dataList = new ArrayList<>();
    private TextView tvPrice;

    public static SettlementFragment getInstance() {
        return new SettlementFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        currView = inflater.inflate(R.layout.frg_settlements, container, false);

        initialize();

        return currView;
    }

    private void initialize() {
        GlobalFunction.getInstance().overrideFonts(getContext(), currView);
        initVariable();
        initList();
        callApiGetSettlement();
    }


    private void initVariable() {
        mRecyclerView = currView.findViewById(R.id.frg_settlement_recycler);
        tvPrice = currView.findViewById(R.id.frg_settlement_tv_price);
        tvPrice.setTypeface(Constants.iranSenseBold);
    }

    private void initList() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new SettlementAdapter(dataList);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void callApiGetSettlement() {

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    JSONArray dataArray = object.getJSONArray(Constants.JSON_DATA);

                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject jsonObject = dataArray.getJSONObject(i);
                        dataList.add(new StructSettlement(
                                jsonObject.getString(Constants.JSON_ORDER_DATE),
                                jsonObject.getString(Constants.JSON_TRANSCATION_AMOUNT),
                                jsonObject.getString(Constants.JSON_TRANSCATION_REFID),
                                jsonObject.getString(Constants.JSON_PAYMENT_STATUS), true));
                    }


                    mAdapter.notifyDataSetChanged();
                } else
                    onError(object.getString(Constants.JSON_MESSAGE));
            }

            public void onError(String string) {
                if (string != null && string.length() > 0 && !string.contains(getString(R.string.spam_message_from_server2)))
                    GlobalFunction.getInstance().toast(getContext(), string);
            }

        };

        new OkHttpHandler(getContext(), Constants.API_GET_ALL_PAYMENT_BY_ID, eventListener)
                .addParam(Constants.JSON_ID, AppPreferences.getInstance().getUserId())
                .send();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getContext() != null)
            callApiGetSettlement();
    }
}
