package nobar.vallcosofweregroup.com.nobardriver.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.AppPreferences;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;
import nobar.vallcosofweregroup.com.nobardriver.global.TemporaryPreferences;
import nobar.vallcosofweregroup.com.nobardriver.network.NetworkEventListener;
import nobar.vallcosofweregroup.com.nobardriver.network.OkHttpHandler;
import nobar.vallcosofweregroup.com.nobardriver.paint.PaintView;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructData;

/**
 * Created by Javad Vatan on 1/12/2018.
 */

public class SignatureActivity extends AppCompatActivity implements View.OnClickListener {
    private PaintView paintView;
    private FrameLayout flConfirm;
    private TextView tvConfirm;
    private ProgressBar pbConfirm;
    private StructData structData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);
        initVariable();
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());

        findViewById(R.id.iv_back).setOnClickListener(this);
        findViewById(R.id.activity_signature_iv_clear).setOnClickListener(this);
        findViewById(R.id.activity_signature_fl_confirm).setOnClickListener(this);
        paintView = findViewById(R.id.activity_signature_paint);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        paintView.init(metrics);
        flConfirm = findViewById(R.id.activity_signature_fl_confirm);
        tvConfirm = findViewById(R.id.activity_signature_tv_confirm);
        pbConfirm = findViewById(R.id.activity_signature_pb_confirm);

        structData = TemporaryPreferences.getInstance().getPrefData();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.activity_signature_iv_clear:
                paintView.clear();
                break;

            case R.id.activity_signature_fl_confirm:
                callApiUploadFile();
                break;
        }
    }

    private void callApiUploadFile() {
        final File fileImage = createFileFromBitmap();
        tvConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {

                    pbConfirm.setVisibility(View.INVISIBLE);
                    tvConfirm.setVisibility(View.VISIBLE);

                    GlobalFunction.getInstance().toast(SignatureActivity.this,
                            object.getString(Constants.JSON_MESSAGE));
                    callApiUpdateOrderDriverStatus(String.valueOf(Constants.STATUS_DRIVER_FINISHED_ORDER));
                    if (fileImage.exists())
                        fileImage.delete();
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                pbConfirm.setVisibility(View.INVISIBLE);
                tvConfirm.setVisibility(View.VISIBLE);
                if (fileImage.exists())
                    fileImage.delete();
                GlobalFunction.getInstance().toast(SignatureActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_UPLOAD_SIGNATURE, eventListener)
                .addImage(fileImage, Constants.JSON_ORDER_ID, TemporaryPreferences.getInstance()
                        .getPrefData().getId())
                .send();
    }

    private File createFileFromBitmap() {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        paintView.getPaintedImage().compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File signatureFile = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            signatureFile.createNewFile();
            fo = new FileOutputStream(signatureFile);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return signatureFile;
    }

    private void callApiUpdateOrderDriverStatus(String statusNumber) {
        tvConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);
        NetworkEventListener networkEventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200) ||
                        object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_201)) {
                    tvConfirm.setVisibility(View.VISIBLE);
                    pbConfirm.setVisibility(View.INVISIBLE);
                    finishedOrder();


                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            @Override
            public void onError(String string) {
                tvConfirm.setVisibility(View.VISIBLE);
                pbConfirm.setVisibility(View.INVISIBLE);
                GlobalFunction.getInstance().toast(SignatureActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_UPDATE_ORDER_DRIVER_STATUS, networkEventListener)
                .addParam(Constants.JSON_ORDER_ID, structData.getId())
                .addParam(Constants.JSON_DRIVER_ID, AppPreferences.getInstance().getUserId())
                .addParam(Constants.JSON_STATUS, statusNumber)
                .send();
    }

    private void finishedOrder() {
        TemporaryPreferences.getInstance().clearPreferences();
        Intent intent = new Intent(SignatureActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        GlobalFunction.getInstance().toast(SignatureActivity.this, getString(R.string.finished_order));
        finish();
    }
}
