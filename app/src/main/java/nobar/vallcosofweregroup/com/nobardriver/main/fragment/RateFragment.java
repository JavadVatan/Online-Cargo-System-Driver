package nobar.vallcosofweregroup.com.nobardriver.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.adapter.RateAdapter;
import nobar.vallcosofweregroup.com.nobardriver.core.BaseFragment;
import nobar.vallcosofweregroup.com.nobardriver.global.AppPreferences;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;
import nobar.vallcosofweregroup.com.nobardriver.network.NetworkEventListener;
import nobar.vallcosofweregroup.com.nobardriver.network.OkHttpHandler;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructRate;

public class RateFragment extends BaseFragment {
    private RecyclerView mRecyclerView;
    private RateAdapter mAdapter;
    private List<StructRate> dataList = new ArrayList<>();
    private TextView tvRank, tvRate;

    public static RateFragment getInstance() {
        return new RateFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        currView = inflater.inflate(R.layout.frg_rate, container, false);

        initialize();

        return currView;
    }

    private void initialize() {
        initVariable();
        initList();
        callApiGetDriverRank();
    }

    private void initVariable() {
        mRecyclerView = currView.findViewById(R.id.frg_rate_recycler);
        GlobalFunction.getInstance().overrideFonts(getContext(), currView);
        tvRank = currView.findViewById(R.id.frg_rate_tv_rank);
        tvRate = currView.findViewById(R.id.frg_rate_tv_rate);

        tvRank.setTypeface(Constants.iranSenseBold);
        tvRate.setTypeface(Constants.iranSenseBold);
    }

    private void initList() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new RateAdapter(dataList);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void callApiGetDriverRank() {
        NetworkEventListener networkEventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200) ||
                        object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_201)) {
                    parseDataApiGetDriverRank(object.getJSONObject(Constants.JSON_DATA)
                            .getJSONArray(Constants.JSON_DATA));
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            @Override
            public void onError(String string) {
                GlobalFunction.getInstance().toast(getContext(), string);
            }
        };

        String url = Constants.API_GET_DRIVER_RANK /*+ AppPreferences.getInstance().getUserId()*/;
        new OkHttpHandler(getActivity(), url, networkEventListener)
                .send();
    }
/*
 "name":"sssssss",
         "family":"asdadad",
         "address":"",
         "mobile":"09364087740",
         "car":"وانت",
         "rank":"0",
         "date":"324234",
         "status":"1"
*/

    private void parseDataApiGetDriverRank(JSONArray array) throws JSONException {
        dataList.clear();
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            dataList.add(new StructRate(object.getString(Constants.JSON_USER_ID),
                    object.getString(Constants.JSON_NAME) + " " +
                            object.getString(Constants.JSON_NAME),
                    object.getString(Constants.JSON_CAR),
                    object.getString(Constants.JSON_RANK),
                    Objects.equals(object.getString(Constants.JSON_USER_ID), AppPreferences.getInstance().getUserId())
            ));
        }
        if (dataList.size() != 0) {
            findCurrUserData();
            mAdapter.notifyDataSetChanged();
        }
    }

    private void findCurrUserData() {
        int userRank = -1;
        String userId = AppPreferences.getInstance().getUserId();
        for (int i = 0; i < dataList.size(); i++)
            if (Objects.equals(dataList.get(i).getUserId(), userId)) {
                userRank = i;
            }

        tvRank.setText(String.valueOf(userRank + 1));
        tvRate.setText(dataList.get(userRank).getGrade());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getContext() != null)
            callApiGetDriverRank();
    }
}
