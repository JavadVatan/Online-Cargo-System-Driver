package nobar.vallcosofweregroup.com.nobardriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructSettlement;

/**
 * Created by Javad Vatan on 11/9/2017.
 */

public class SettlementAdapter extends RecyclerView.Adapter<SettlementAdapter.ViewHolder> {
    private List<StructSettlement> dataList;
    private Context mContext;

    public SettlementAdapter(List<StructSettlement> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_settelment, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.tvDate.setText(dataList.get(position).getDate());
        holder.tvOrder.setText(dataList.get(position).getOrder());
        holder.tvStatus.setText(dataList.get(position).getStatus());
        holder.tvPrice.setText(dataList.get(position).getPrice());
        manageColors(holder, position);

    }

    private void manageColors(final ViewHolder holder, int position) {
        if (dataList.get(position).isProfit()) {
            holder.flStatus.setBackgroundColor(mContext.getResources().getColor(R.color.item_settlement_profit));
            holder.tvPrice.setTextColor(mContext.getResources().getColor(R.color.item_settlement_profit));
            holder.ivStatus.setImageResource(R.drawable.ic_settelment_profit);
        } else {
            holder.flStatus.setBackgroundColor(mContext.getResources().getColor(R.color.item_settlement_damage));
            holder.tvPrice.setTextColor(mContext.getResources().getColor(R.color.item_settlement_damage));
            holder.ivStatus.setImageResource(R.drawable.ic_settlement_damage);
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDate, tvOrder, tvStatus, tvPrice;
        private FrameLayout flStatus;
        private ImageView ivStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.item_settlement_tv_date);
            tvOrder = itemView.findViewById(R.id.item_settlement_tv_order);
            tvStatus = itemView.findViewById(R.id.item_settlement_tv_status);
            tvPrice = itemView.findViewById(R.id.item_settlement_tv_price);
            flStatus = itemView.findViewById(R.id.item_settlement_fl_status);
            ivStatus = itemView.findViewById(R.id.item_settlement_iv_status);

            tvDate.setTypeface(Constants.iranSenseLight);
            tvStatus.setTypeface(Constants.iranSenseLight);

            tvPrice.setTypeface(Constants.iranSenseBold);
            tvOrder.setTypeface(Constants.iranSenseBold);

        }


    }
}
