package nobar.vallcosofweregroup.com.nobardriver.struct;

/**
 * Created by Javad Vatan on 11/10/2017.
 */

public class StructSettlement {
    private String date, price, order, status;
    private boolean isProfit;

    public StructSettlement(String date, String price, String order, String status, boolean isProfit) {
        this.date = date;
        this.price = price;
        this.order = order;
        this.status = status;
        this.isProfit = isProfit;
    }

    public StructSettlement() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isProfit() {
        return isProfit;
    }

    public void setProfit(boolean profit) {
        isProfit = profit;
    }
}
