package nobar.vallcosofweregroup.com.nobardriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructRate;

/**
 * Created by Javad Vatan on 11/9/2017.
 */

public class RateAdapter extends RecyclerView.Adapter<RateAdapter.ViewHolder> {
    private List<StructRate> dataList;
    private Context mContext;

    public RateAdapter(List<StructRate> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rate, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.tvRate.setText(dataList.get(position).getGrade());
        holder.tvNumber.setText((position + 1) + "");
        holder.tvRate.setText(Html.fromHtml(dataList.get(position).getGrade()));
        holder.tvCarName.setText(dataList.get(position).getCarName());
        holder.tvDriverName.setText(dataList.get(position).getDriverName());

        manageIconRate(holder, position);

    }

    private void manageIconRate(final ViewHolder holder, int position) {
        if (dataList.get(position).isCurrDriver())
            holder.tvNumber.setBackgroundResource(R.drawable.bg_grade_active);
        else
            holder.tvNumber.setBackgroundResource(R.drawable.bg_grade_deactive);

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDriverName, tvCarName, tvRate, tvNumber;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDriverName = itemView.findViewById(R.id.item_rate_tv_driver_name);
            tvCarName = itemView.findViewById(R.id.item_rate_tv_car_name);
            tvNumber = itemView.findViewById(R.id.item_rate_tv_rate_number);
            tvRate = itemView.findViewById(R.id.item_rate_tv_rate);


            tvDriverName.setTypeface(Constants.iranSenseLight);
            tvCarName.setTypeface(Constants.iranSenseLight);
            tvNumber.setTypeface(Constants.iranSenseLight);
            tvRate.setTypeface(Constants.iranSenseLight);
        }
    }
}
