package nobar.vallcosofweregroup.com.nobardriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.ConvertDate;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructHistory;

/**
 * Created by Javad Vatan on 11/9/2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private List<StructHistory> dataList;
    private Context mContext;

    public HistoryAdapter(List<StructHistory> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.tvOrigin.setText(dataList.get(position).getOriginAddress());
        holder.tvDestination.setText(dataList.get(position).getDestiantionAddre());
        holder.tvOrder.setText("#" + dataList.get(position).getOrder());
        holder.tvStatus.setText(dataList.get(position).getStatus());
        convertDate(holder, position);

    }

    private void convertDate(final ViewHolder holder, int position) {
        try {
            long dateInMillis = Long.parseLong(dataList.get(position).getDate());
            ConvertDate convertdate = ConvertDate.getInstance();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(dateInMillis);
            convertdate.calcGregorian(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            int[] date = convertdate.GetPersianDate();
            String dateAsString = date[0] + " / " + date[1] + " / " + date[2];
            holder.tvDate.setText(dateAsString);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvOrigin, tvDestination, tvDate, tvOrder, tvStatus, tvOriginTitle,
                tvDestinationTitle, tvDateTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.item_history_tv_date);
            tvDestination = itemView.findViewById(R.id.item_history_tv_destination);
            tvOrigin = itemView.findViewById(R.id.item_history_tv_origin);
            tvOrder = itemView.findViewById(R.id.item_history_tv_order);
            tvStatus = itemView.findViewById(R.id.item_history_tv_status);
            tvDateTitle = itemView.findViewById(R.id.item_history_tv_date_title);
            tvDestinationTitle = itemView.findViewById(R.id.item_history_tv_destination_title);
            tvOriginTitle = itemView.findViewById(R.id.item_history_tv_origin_title);

            tvOrigin.setTypeface(Constants.iranSenseLight);
            tvDestination.setTypeface(Constants.iranSenseLight);
            tvDate.setTypeface(Constants.iranSenseLight);
            tvOriginTitle.setTypeface(Constants.iranSenseLight);
            tvDestinationTitle.setTypeface(Constants.iranSenseLight);
            tvDateTitle.setTypeface(Constants.iranSenseLight);
            tvStatus.setTypeface(Constants.iranSenseLight);
            tvOrder.setTypeface(Constants.iranSenseBold);

        }


    }
}
