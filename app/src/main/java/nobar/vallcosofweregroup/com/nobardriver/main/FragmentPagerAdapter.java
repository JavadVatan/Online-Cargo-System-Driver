package nobar.vallcosofweregroup.com.nobardriver.main;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.main.fragment.HistoryFragment;
import nobar.vallcosofweregroup.com.nobardriver.main.fragment.HomeFragment;
import nobar.vallcosofweregroup.com.nobardriver.main.fragment.RateFragment;
import nobar.vallcosofweregroup.com.nobardriver.main.fragment.SettlementFragment;


public class FragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

    public static final int PAGE_MAIN = 1;

    private AppCompatActivity mParentActivity;
    private int count, page;

    public FragmentPagerAdapter(AppCompatActivity parentActivity, int count, int page) {
        super(parentActivity.getSupportFragmentManager());
        mParentActivity = parentActivity;
        this.page = page;
        this.count = count;
    }

    @Override
    public Fragment getItem(int position) {
        switch (page) {
            case PAGE_MAIN:
                switch (position) {
                    case Constants.TAB_STAR:
                        return RateFragment.getInstance();

                    case Constants.TAB_SETTLEMENT:
                        return SettlementFragment.getInstance();

                    /*case Constants.TAB_DOING:
                        return DoingFragment.getInstance();
*/
                    case Constants.TAB_HISTORY:
                        return HistoryFragment.getInstance();

                    case Constants.TAB_HOME:
                        return HomeFragment.getInstance();
                }
                break;

        }
        return null;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}