package nobar.vallcosofweregroup.com.nobardriver.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.ConvertDate;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructData;
import nobar.vallcosofweregroup.com.nobardriver.util.Util;

/**
 * Created by Javad Vatan on 11/9/2017.
 */

public abstract class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    private List<StructData> dataList;
    private Context mContext;

    public OrderAdapter(List<StructData> dataList) {
        this.dataList = dataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.tvOrigin.setText(dataList.get(position).getStartAddressTitle());
        holder.tvDestination.setText(dataList.get(position).getEndAddressTitle());
        holder.tvOrder.setText(String.format("#%s", dataList.get(position).getId()));
        holder.tvPrice.setText(String.format("%s ريال ", Util.getDecimalFormattedString(dataList.get(position).getPrice())));
        convertDate(holder, position);
    }

    private void convertDate(final ViewHolder holder, int position) {
        try {
            long dateInMillis = Long.parseLong(dataList.get(position).getDate());
            ConvertDate convertdate = ConvertDate.getInstance();
            Calendar calendar = Calendar.getInstance();

            calendar.setTimeInMillis(dateInMillis);
            convertdate.calcGregorian(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            int[] date = convertdate.GetPersianDate();
            String dateAsString = date[0] + " / " + date[1] + " / " + date[2] + " | "
                    + calendar.get(Calendar.HOUR_OF_DAY) + " : " + calendar.get(Calendar.MINUTE);
            holder.tvDate.setText(dateAsString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public abstract void onOrderClicked(int position);

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvOrigin, tvDestination, tvDate, tvOrder, tvPrice, tvOriginTitle,
                tvDestinationTitle, tvDateTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.item_order_tv_date);
            tvDestination = itemView.findViewById(R.id.item_order_tv_destination);
            tvOrigin = itemView.findViewById(R.id.item_order_tv_origin);
            tvOrder = itemView.findViewById(R.id.item_order_tv_order);
            tvPrice = itemView.findViewById(R.id.item_order_tv_price);
            tvDateTitle = itemView.findViewById(R.id.item_order_tv_date_title);
            tvDestinationTitle = itemView.findViewById(R.id.item_order_tv_destination_title);
            tvOriginTitle = itemView.findViewById(R.id.item_order_tv_origin_title);

            tvOrigin.setTypeface(Constants.iranSenseLight);
            tvDestination.setTypeface(Constants.iranSenseLight);
            tvDate.setTypeface(Constants.iranSenseLight);
            tvOriginTitle.setTypeface(Constants.iranSenseLight);
            tvDestinationTitle.setTypeface(Constants.iranSenseLight);
            tvDateTitle.setTypeface(Constants.iranSenseLight);
            tvPrice.setTypeface(Constants.iranSenseBold);
            tvOrder.setTypeface(Constants.iranSenseBold);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onOrderClicked(getAdapterPosition());
                }
            });
        }


    }
}
