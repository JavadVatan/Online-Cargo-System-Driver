package nobar.vallcosofweregroup.com.nobardriver.struct;

/**
 * Created by Javad Vatan on 9/11/2017.
 */

public class StructRate {
    private String driverName, carName, grade, userId;
    private boolean isCurrDriver;

    public StructRate(String userId, String driverName, String carName, String grade, boolean isCurrDriver) {
        this.userId = userId;
        this.driverName = driverName;
        this.carName = carName;
        this.grade = grade;
        this.isCurrDriver = isCurrDriver;
    }

    public StructRate() {

    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public boolean isCurrDriver() {
        return isCurrDriver;
    }

    public void setCurrDriver(boolean currDriver) {
        isCurrDriver = currDriver;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}


