package nobar.vallcosofweregroup.com.nobardriver.global;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class Application extends android.app.Application {

    private static volatile Application instance;
    private volatile Context mContext;

    public static synchronized Application getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        mContext = getApplicationContext();
    }


    public Context getContext() {
        return mContext;
    }

    public boolean isConnectionAnyInternet() {
        ConnectivityManager conMgr = (ConnectivityManager) mContext.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfoWiFi, networkInfoMobile;
        networkInfoMobile = conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        networkInfoWiFi = conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return (networkInfoWiFi != null &&
                networkInfoWiFi.getState() == NetworkInfo.State.CONNECTED) ||
                (networkInfoMobile != null &&
                        networkInfoMobile.getState() == NetworkInfo.State.CONNECTED);
    }

}
