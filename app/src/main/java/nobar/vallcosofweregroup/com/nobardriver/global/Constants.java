package nobar.vallcosofweregroup.com.nobardriver.global;

import android.graphics.Typeface;

/**
 * Created by MikroCom on 12/07/2017.
 */

public class Constants {

    public static final int SLIDER_DELAY = 5500;

    /**
     * Server Urls
     */
    public static final String BASE_URL = "http://nobaar.com/apibar/public/";
    public static final String NETWORK_ACTION_LOGIN = BASE_URL + "driver_login";
    public static final String NETWORK_ACTION_GET_MESSAGE = BASE_URL + "messages_driver";
    public static final String NETWORK_ACTION_ACTIVATION = BASE_URL + "send_activeCode/";
    public static final String NETWORK_TRANSPORTATION_HISTORY = BASE_URL + "driver_transportation_history ";
    public static final String NETWORK_ACTION_GET_ORDER = BASE_URL + "all_active_order";
    public static final String API_SEND_PROFILE = BASE_URL + "send_profile";
    public static final String API_UPLOAD_PROFILE = BASE_URL + "upload_profile";
    public static final String API_UPDATE_ORDER_DRIVER_STATUS = BASE_URL + "update_order_driver_status";
    public static final String API_CANCEL_ORDER = BASE_URL + "cancel_order";
    public static final String API_GET_ORDER_STATUS = BASE_URL + "get_order_status/";
    public static final String API_GET_LAST_DRIVER_STATUS = BASE_URL + "get_last_driver_order";
    public static final String API_GET_DRIVER_RANK = BASE_URL + "get_drivers_rank";
    public static final String API_GET_ORDER_BY_ORDER_ID = BASE_URL + "get_order_by_order_id/";
    public static final String API_GET_ALL_PAYMENT_BY_ID = BASE_URL + "get_all_payment_by_id";
    public static final String API_UPLOAD_SIGNATURE = BASE_URL + "upload_signature";


    // http://top-tax.ir/apibar/public/get_order_driver_by_id
    /**
     * Json Keys
     */
    public static final String JSON_STATUS = "status";
    public static final String JSON_STATUS_200 = "200";
    public static final String JSON_STATUS_201 = "201";
    public static final String JSON_STATUS_401 = "401";
    public static final String JSON_STATUS_402 = "402";
    public static final String JSON_MESSAGE = "message";
    public static final String JSON_DATA = "data";
    public static final String JSON_USER_ID_LOWER_CASE = "user_id";
    public static final String JSON_IS_FIRST = "is_first";
    public static final String JSON_IMAGE = "image";
    public static final String JSON_START_ADDRESS_TITLE = "startAddressTitle";
    public static final String JSON_START_ADDRESS_LAT = "startAddressLat";
    public static final String JSON_START_ADDRESS_LNG = "startAddressLong";
    public static final String JSON_END_ADDRESS_TITLE = "endAddressTitle";
    public static final String JSON_END_ADDRESS_LAT = "endAddressLat";
    public static final String JSON_END_ADDRESS_LNG = "endAddressLong";
    public static final String JSON_ID = "id";
    public static final String JSON_COUNT = "count";
    public static final String JSON_SUBJECT = "subject";
    public static final String JSON_ORDER_ID = "order_id";
    public static final String JSON_DATE = "date";
    public static final String JSON_PRICE = "price";
    public static final String JSON_MOBILE = "mobile";
    public static final String JSON_EMAIL = "email";
    public static final String JSON_NAME = "name";
    public static final String JSON_FAMILY = "family";
    public static final String JSON_USER_ID = "user_id";
    public static final String JSON_CAR_ID = "car_id";
    public static final String JSON_TITLE = "title";
    public static final String JSON_PAY_WITH_RECEIVER = "pay_with_reciver";
    public static final String JSON_RECEIVER_NAME = "reciver_name";
    public static final String JSON_RECEIVER_MOBILE = "reciver_mobile";
    public static final String JSON_VASAYELE_HAZINEHDAR = "vasayeleHazinedar";
    public static final String JSON_TIME = "time";
    public static final String JSON_BARBER_WORKER = "barbar_worker";
    public static final String JSON_CHIDEMAN_WORKER = "chideman_worker";
    public static final String JSON_ORIGIN_FLOOR = "origin_floor";
    public static final String JSON_ORIGIN_WALIKING = "origin_walking";
    public static final String JSON_DESTINATION_FLOOR = "destination_floor";
    public static final String JSON_DESTINATION_WALKING = "destination_walking";
    public static final String JSON_USER_NAME = "username";
    public static final String JSON_PASSWORD = "password";
    public static final String JSON_DRIVER_ID = "driver_id";
    public static final String JSON_CAR = "car";
    public static final String JSON_ADDRESS = "address";
    public static final String JSON_RANK = "rank";
    public static final String JSON_TRANSCATION_AMOUNT = "transaction_amount";
    public static final String JSON_TRANSCATION_REFID = "transaction_refid";
    public static final String JSON_TRANSCATION_DATE = "transaction_date";
    public static final String JSON_PAYMENT_STATUS = "payment_status";
    public static final String JSON_ORDER_DATE = "order_date";
    public static final String JSON_TIME_CAL = "timeCalc";


    /**
     * Order Status
     */
    public static final String STATUS_DRIVER_CANCEL_ORDER = "6";
    public static final String STATUS_DRIVER_ACCEPT_ORDER = "2";
    public static final String STATUS_DRIVER_ACCEPT_DETAIL_ORDER = "3";
    public static final String STATUS_DRIVER_START_PROCESS = "4";
    public static final String STATUS_DRIVER_END_PROCESS = "5";
    public static final int STATUS_DRIVER_FINISHED_ORDER = 9;


    /**
     * Tab Orders
     */

    public static final int TAB_STAR = 0;
    public static final int TAB_SETTLEMENT = 1;
    // public static final int TAB_DOING = 2;
    public static final int TAB_HISTORY = 2;
    public static final int TAB_HOME = 3;


    /**
     * Key Preferences
     */

    public static final String Preferences_USER_LOGIN = "user_login";
    public static final String Preferences_USER_ID = "user_id";
    public static final String Preferences_USER_PHONE = "user_phone";
    public static final String Preferences_USER_COST = "user_cost";
    public static final String Preferences_IS_FIRST = "is_first";
    public static final String Preferences_USER_NAME = "user_name";
    public static final String Preferences_USER_FAMILY = "user_family";
    public static final String Preferences_USER_CREDIT = "user_credit";
    public static final String Preferences_USER_EMAIL = "user_email";
    public static final String Preferences_USER_PHOTO_URL = "user_photo_url";
    public static final String Preferences_DRIVER_ACTIVE = "driver_active";

    /**
     * Image Profile
     */
    public static final String IMAGE_SAVE_NAME_PROFILE = "nobar_driver_image.png";
    public static final String IMAGE_SAVE_FOLDER = "nobar";
    public static final int IMAGE_SIZE = 250;
    /**
     * Fonts
     */
    public static Typeface iranSenseLight;
    public static Typeface iranSenseBold;

}
