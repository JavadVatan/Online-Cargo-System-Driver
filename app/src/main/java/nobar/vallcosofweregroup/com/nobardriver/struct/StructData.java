package nobar.vallcosofweregroup.com.nobardriver.struct;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Javad Vatan on 9/11/2017.
 */

public class StructData {

    private String startAddressTitle;
    private String startAddressLng;
    private String startAddressLat;
    private String endAddressTitle;
    private String endAddressLng;
    private String endAddressLat;
    private String reciver_name;
    private String status;
    private String car_id;
    private String pay_with_reciver;
    private String destination_floor;
    private String origin_walking;
    private List<StructExpensiveTools> vasayeleHazinedar = new ArrayList<>();
    private String date;
    private String id;
    private String destination_walking;
    private String title;
    private String reciver_mobile;
    private String time;
    private String price;
    private String origin_floor;
    private String barbar_worker;
    private String user_id;
    private String chideman_worker;

    public String getStartAddressTitle() {
        return startAddressTitle;
    }

    public void setStartAddressTitle(String startAddressTitle) {
        this.startAddressTitle = startAddressTitle;
    }

    public String getReciver_name() {
        return reciver_name;
    }

    public void setReciver_name(String reciver_name) {
        this.reciver_name = reciver_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCar_id() {
        return car_id;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    public String getPay_with_reciver() {
        return pay_with_reciver;
    }

    public void setPay_with_reciver(String pay_with_reciver) {
        this.pay_with_reciver = pay_with_reciver;
    }

    public String getDestination_floor() {
        return destination_floor;
    }

    public void setDestination_floor(String destination_floor) {
        this.destination_floor = destination_floor;
    }

    public String getOrigin_walking() {
        return origin_walking;
    }

    public void setOrigin_walking(String origin_walking) {
        this.origin_walking = origin_walking;
    }

    public List<StructExpensiveTools> getVasayeleHazinedar() {
        return vasayeleHazinedar;
    }

    public void setVasayeleHazinedar(List<StructExpensiveTools> vasayeleHazinedar) {
        this.vasayeleHazinedar = vasayeleHazinedar;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDestination_walking() {
        return destination_walking;
    }

    public void setDestination_walking(String destination_walking) {
        this.destination_walking = destination_walking;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReciver_mobile() {
        return reciver_mobile;
    }

    public void setReciver_mobile(String reciver_mobile) {
        this.reciver_mobile = reciver_mobile;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOrigin_floor() {
        return origin_floor;
    }

    public void setOrigin_floor(String origin_floor) {
        this.origin_floor = origin_floor;
    }

    public String getBarbar_worker() {
        return barbar_worker;
    }

    public void setBarbar_worker(String barbar_worker) {
        this.barbar_worker = barbar_worker;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getChideman_worker() {
        return chideman_worker;
    }

    public void setChideman_worker(String chideman_worker) {
        this.chideman_worker = chideman_worker;
    }

    public String getEndAddressTitle() {
        return endAddressTitle;
    }

    public void setEndAddressTitle(String endAddressTitle) {
        this.endAddressTitle = endAddressTitle;
    }

    public String getStartAddressLng() {
        return startAddressLng;
    }

    public void setStartAddressLng(String startAddressLng) {
        this.startAddressLng = startAddressLng;
    }

    public String getStartAddressLat() {
        return startAddressLat;
    }

    public void setStartAddressLat(String startAddressLat) {
        this.startAddressLat = startAddressLat;
    }

    public String getEndAddressLng() {
        return endAddressLng;
    }

    public void setEndAddressLng(String endAddressLng) {
        this.endAddressLng = endAddressLng;
    }

    public String getEndAddressLat() {
        return endAddressLat;
    }

    public void setEndAddressLat(String endAddressLat) {
        this.endAddressLat = endAddressLat;
    }
}