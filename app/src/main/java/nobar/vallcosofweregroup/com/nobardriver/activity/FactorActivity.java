package nobar.vallcosofweregroup.com.nobardriver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.adapter.DetailFactorAdapter;
import nobar.vallcosofweregroup.com.nobardriver.global.AppPreferences;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;
import nobar.vallcosofweregroup.com.nobardriver.global.TemporaryPreferences;
import nobar.vallcosofweregroup.com.nobardriver.network.NetworkEventListener;
import nobar.vallcosofweregroup.com.nobardriver.network.OkHttpHandler;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructData;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructDetailFactor;
import nobar.vallcosofweregroup.com.nobardriver.util.Util;

/**
 * Created by Javad Vatan on 10/19/2017.
 */

public class FactorActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG_MODE = "mode_page";
    public static final int TAG_MODE_NORMAL = 0;
    public static final int TAG_MODE_REVIEW = 1;
    public static final int TAG_MODE_FINAL = 2;
    private TextView tvConfirm, tvPrice, tvAddressOrigin, tvAddressDestination, tvDate,
            tvReceiverProperty, tvDiscount;
    private RecyclerView rvDetailFactor;
    private ProgressBar pbPrice, pbConfirm;
    private String currOrderId = null;
    private StructData structData;
    private int currMode = TAG_MODE_NORMAL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temporary_factor);
        initBundle();
        initVariable();
        initList();
        setDataText();
        /*    callApiSendOrder();*/
    }

    private void initBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            currMode = bundle.getInt(TAG_MODE);
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        structData = TemporaryPreferences.getInstance().getPrefData();
        rvDetailFactor = findViewById(R.id.activity_factor_rv_detail_factor);
        tvAddressOrigin = findViewById(R.id.activity_factor_tv_address_origin);
        tvAddressDestination = findViewById(R.id.activity_factor_tv_address_destination);
        tvDate = findViewById(R.id.activity_factor_tv_loading_date);
        tvReceiverProperty = findViewById(R.id.activity_factor_tv_receiver_property);
        tvPrice = findViewById(R.id.activity_factor_tv_price);
        pbPrice = findViewById(R.id.activity_factor_pb_price);

        tvDiscount = findViewById(R.id.activity_factor_tv_discount);
        pbConfirm = findViewById(R.id.activity_factor_pb_confirm);
        tvConfirm = findViewById(R.id.activity_factor_tv_confirm);
        tvConfirm.setOnClickListener(this);
        findViewById(R.id.activity_factor_fl_path_on_map).setOnClickListener(this);

        setPageModeText();
    }

    private void setPageModeText() {
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) tvPrice.getLayoutParams();
        params.gravity = Gravity.CENTER;

        switch (currMode) {
            case TAG_MODE_NORMAL:
                ((TextView) findViewById(R.id.include_app_bar_tv_title)).setText(R.string.temporary_factor);
                ((TextView) findViewById(R.id.activity_factor_tv_confirm)).setText(R.string.accepts_order);
                tvPrice.setLayoutParams(params);
                pbPrice.setLayoutParams(params);
                break;

            case TAG_MODE_REVIEW:
            /*    ((TextView) findViewById(R.id.include_app_bar_tv_title)).setText(R.string.factor);
                ((TextView) findViewById(R.id.activity_factor_tv_confirm)).setText(R.string.back);
                tvPrice.setLayoutParams(params);
                pbPrice.setLayoutParams(params);*/
                break;

            case TAG_MODE_FINAL:
                ((TextView) findViewById(R.id.include_app_bar_tv_title)).setText(R.string.factor_final);
                ((TextView) findViewById(R.id.activity_factor_tv_confirm)).setText(R.string.customer_signature);
                findViewById(R.id.include_app_bar_iv_refresh).setVisibility(View.VISIBLE);
                findViewById(R.id.include_app_bar_iv_refresh).setOnClickListener(this);
                tvDiscount.setVisibility(View.VISIBLE);
                tvDiscount.setText(R.string.pay_with_credit);
                params.gravity = Gravity.LEFT;
                tvPrice.setLayoutParams(params);
                break;
        }
    }

    private void setDataText() {
        tvAddressOrigin.setText(structData.getStartAddressTitle());
        tvAddressDestination.setText(structData.getEndAddressTitle());
        tvDate.setText(convertDate());
        tvReceiverProperty.setText(structData.getReciver_name()
                + " | " + structData.getReciver_mobile());
        mangeSetPrice();
    }

    private void mangeSetPrice() {
        switch (currMode) {
            case TAG_MODE_NORMAL:
            case TAG_MODE_REVIEW:
                tvPrice.setText(Util.getDecimalFormattedString(structData.getPrice()) + " ريال ");
                break;

            case TAG_MODE_FINAL:
                callApiGetOrderByOrderId();
                break;
        }
    }

    private void callApiGetOrderByOrderId() {
        tvPrice.setVisibility(View.INVISIBLE);
        pbPrice.setVisibility(View.VISIBLE);

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    JSONObject mainObject = object.getJSONArray(Constants.JSON_DATA).getJSONObject(0);
                    pbPrice.setVisibility(View.INVISIBLE);
                    tvPrice.setVisibility(View.VISIBLE);
                    tvPrice.setText(String.format("%s ريال ", Util.getDecimalFormattedString(mainObject.getString(Constants.JSON_PRICE))));
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                pbPrice.setVisibility(View.INVISIBLE);
                tvPrice.setVisibility(View.VISIBLE);
                GlobalFunction.getInstance().toast(FactorActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_GET_ORDER_BY_ORDER_ID + structData.getId(), eventListener)
                .send();
    }

    private String convertDate() {
        try {
            long dateInMillis = Long.parseLong(structData.getDate());
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(dateInMillis);
            String dateAsString = calendar.get(Calendar.YEAR) + " / "
                    + calendar.get(Calendar.MONTH) + " / " +
                    calendar.get(Calendar.DAY_OF_MONTH) + " | " +
                    calendar.get(Calendar.HOUR_OF_DAY) + " : " +
                    calendar.get(Calendar.MINUTE);
            return dateAsString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private void initList() {
        List<StructDetailFactor> dataList = new ArrayList<>();
        dataList.add(new StructDetailFactor(getString(R.string.carry_worker), structData.getBarbar_worker()));
        dataList.add(new StructDetailFactor(getString(R.string.cat_worker), structData.getChideman_worker()));
        dataList.add(new StructDetailFactor(getString(R.string.walking_origin_long), structData.getOrigin_walking()));
        dataList.add(new StructDetailFactor(getString(R.string.walking_destination_long), structData.getDestination_walking()));
        dataList.add(new StructDetailFactor(getString(R.string.conut_stair_orgin), structData.getOrigin_floor()));
        dataList.add(new StructDetailFactor(getString(R.string.coint_stair_detination), structData.getDestination_floor()));
        for (int i = 0; i < structData.getVasayeleHazinedar().size(); i++) {
            dataList.add(new StructDetailFactor(structData.getVasayeleHazinedar().get(i).getTitle(),
                    structData.getVasayeleHazinedar().get(i).getCount()));
        }

        DetailFactorAdapter mAdapter = new DetailFactorAdapter(dataList);
        rvDetailFactor.setHasFixedSize(true);
        rvDetailFactor.setLayoutManager(new LinearLayoutManager(this));
        rvDetailFactor.setNestedScrollingEnabled(false);
        rvDetailFactor.setAdapter(mAdapter);
    }

    private void manageConfirm() {

        switch (currMode) {
            case TAG_MODE_NORMAL:
                callApiUpdateOrderDriverStatus();
                break;

            case TAG_MODE_REVIEW:
                finish();
                break;

            case TAG_MODE_FINAL:
                startActivity(new Intent(this, SignatureActivity.class));
                break;
        }
    }

    private void callApiUpdateOrderDriverStatus() {
        tvConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);
        NetworkEventListener networkEventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200) ||
                        object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_201)) {
                    tvConfirm.setVisibility(View.VISIBLE);
                    pbConfirm.setVisibility(View.INVISIBLE);
                    TemporaryPreferences.getInstance().addPreBool(TemporaryPreferences.PREF_IS_ACTIVE_ORDER, true);
                    startActivity(new Intent(FactorActivity.this,
                            RequestActivity.class));
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            @Override
            public void onError(String string) {
                tvConfirm.setVisibility(View.VISIBLE);
                pbConfirm.setVisibility(View.INVISIBLE);
                GlobalFunction.getInstance().toast(FactorActivity.this, string);

            }
        };


        new OkHttpHandler(this, Constants.API_UPDATE_ORDER_DRIVER_STATUS, networkEventListener)
                .addParam(Constants.JSON_ORDER_ID, structData.getId())
                .addParam(Constants.JSON_DRIVER_ID, AppPreferences.getInstance().getUserId())
                .addParam(Constants.JSON_STATUS, Constants.STATUS_DRIVER_ACCEPT_ORDER)
                .send();
    }

    private void storeOrderId(String orderId) {
        TemporaryPreferences temporaryPreferences = TemporaryPreferences.getInstance();
        temporaryPreferences.addPrefString(TemporaryPreferences.PREF_ORDER_ID, orderId);
        currOrderId = orderId;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_factor_tv_confirm:
                manageConfirm();
                break;
            case R.id.activity_factor_fl_path_on_map:
                manageShowPathOnMap();
                break;
            case R.id.include_app_bar_iv_refresh:
                callApiGetOrderStatus();
                GlobalFunction.getInstance().toast(FactorActivity.this, getString(R.string.updating));
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (currMode == TAG_MODE_FINAL)
            callApiGetOrderStatus();
    }

    private void callApiGetOrderStatus() {

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {

                    JSONObject jsonObject = object.getJSONObject(Constants.JSON_DATA);

                    if (Integer.parseInt(jsonObject.getString(Constants.JSON_STATUS)) == 8)
                        manageFinishOrder();

                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                if (string != null && string.length() > 0 && !string.equals("اطلاعاتی موجود نیست"))
                    GlobalFunction.getInstance().toast(FactorActivity.this, string);
            }
        };

        String url = Constants.API_GET_ORDER_STATUS + AppPreferences.getInstance().getUserId();

        new OkHttpHandler(this, url, eventListener)
                .send();
    }

    private void manageFinishOrder() {
        TemporaryPreferences.getInstance().clearPreferences();
        Intent intent = new Intent(FactorActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void manageShowPathOnMap() {
        Intent mapIntent = new Intent(this, MapActivity.class);
        double lat = Double.parseDouble(structData.getEndAddressLat());
        double lng = Double.parseDouble(structData.getEndAddressLng());

        mapIntent.putExtra(MapActivity.TAG_MAP_LAT, lat);
        mapIntent.putExtra(MapActivity.TAG_MAP_LNG, lng);
        startActivity(mapIntent);
    }
/*
    private void callApiAcceptOrder() {
        tvConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);
        new OkHttpHandler(this, Constants.NETWORK_ACTION_ACCEPT_ORDER) {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {
                    pbConfirm.setVisibility(View.INVISIBLE);
                    tvConfirm.setVisibility(View.VISIBLE);

                    JSONArray objectArray = object.getJSONArray(Constants.JSON_DATA);
                    for (int i = 0; i < objectArray.length(); i++) {
                        JSONObject jsonObject = objectArray.getJSONObject(i);
                        storeOrderId(jsonObject.getString(Constants.JSON_ID));

                    }
                    startActivity(new Intent(FactorActivity.this, RequestActivity.class));
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                pbPrice.setVisibility(View.INVISIBLE);
                tvPrice.setVisibility(View.VISIBLE);
                tvPrice.setDataText(R.string.error_in_calculate_price);

                GlobalFunction.getInstance().toast(FactorActivity.this, string);
            }
        }.addParam(Constants.JSON_ORDER_ID, currOrderId).send();
    }*/

}