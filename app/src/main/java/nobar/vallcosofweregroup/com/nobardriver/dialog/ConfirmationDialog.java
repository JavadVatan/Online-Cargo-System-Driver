package nobar.vallcosofweregroup.com.nobardriver.dialog;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;

public class ConfirmationDialog extends AlertDialog implements View.OnClickListener {
    private Context mContext;
    private TextView dialog_tvTitle, dialog_tv_Message;
    private Button dialog_btnPositive, dialog_btnNegative, dialog_btnMore;
    private DialogConfirmationHandler interfaceEventListener = null;


    public ConfirmationDialog(Context context) {
        super(context);
        mContext = context;
    }

    public ConfirmationDialog setDataDialog(DialogConfirmationHandler listener, String title, String message, String negative, String positive) {
        initVariableForErrorDialog();
        interfaceEventListener = listener;
        dialog_tvTitle.setText(title);
        dialog_tv_Message.setText(message);
        dialog_btnPositive.setText(positive);
        dialog_btnNegative.setText(negative);

        if (negative != null || negative != "") {
            dialog_btnNegative.setVisibility(View.VISIBLE);
            dialog_btnNegative.setText(negative);
            dialog_btnNegative.setOnClickListener(this);
        }
        if (positive != null || positive != "") {
            dialog_btnPositive.setVisibility(View.VISIBLE);
            dialog_btnPositive.setText(positive);
            dialog_btnPositive.setOnClickListener(this);
        }
        return this;
    }

    public ConfirmationDialog setHasMoreBtn(String StrMore) {
        dialog_btnMore.setVisibility(View.VISIBLE);
        dialog_btnMore.setText(StrMore);
        dialog_btnMore.setOnClickListener(this);
        return this;
    }

    public ConfirmationDialog onCreateDialog() {
        show();
        return this;
    }

    public ConfirmationDialog setCancelableDialog(boolean status, OnCancelListener listener) {
        setCancelable(status);
        setOnCancelListener(listener);
        return this;
    }

    public ConfirmationDialog setCanceledOnTouchOutsideDialog(boolean status) {
        setCanceledOnTouchOutside(status);
        return this;
    }

    private void initVariableForErrorDialog() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_alert, null);
        dialog_btnPositive = dialogView.findViewById(R.id.dialog_btn_confirm);
        dialog_btnNegative = dialogView.findViewById(R.id.dialog_btn_negative);
        dialog_btnMore = dialogView.findViewById(R.id.dialog_btn_more);
        dialog_tv_Message = dialogView.findViewById(R.id.dialog_tv_message);
        dialog_tvTitle = dialogView.findViewById(R.id.dialog_tv_title);
        dialog_btnPositive.setTypeface(Constants.iranSenseLight);
        dialog_btnNegative.setTypeface(Constants.iranSenseLight);
        dialog_btnMore.setTypeface(Constants.iranSenseLight);
        dialog_tv_Message.setTypeface(Constants.iranSenseLight);
        dialog_tvTitle.setTypeface(Constants.iranSenseBold);

        dialog_btnPositive.setVisibility(View.GONE);
        dialog_btnNegative.setVisibility(View.GONE);
        dialog_btnMore.setVisibility(View.GONE);

        setView(dialogView);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        destroyDialog();
    }

    private void destroyDialog() {
        dismiss();
        interfaceEventListener.onNegativeDialogConfirmationClicked();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_btn_negative:
                destroyDialog();
                break;

            case R.id.dialog_btn_confirm:
                dismiss();
                interfaceEventListener.onPositiveDialogConfirmationClicked();
                break;
            case R.id.dialog_btn_more:
                dismiss();
                interfaceEventListener.onMoreDialogConfirmationClicked();
                break;
        }
    }

    public interface DialogConfirmationHandler {
        void onPositiveDialogConfirmationClicked();

        void onNegativeDialogConfirmationClicked();

        void onMoreDialogConfirmationClicked();
    }
}

