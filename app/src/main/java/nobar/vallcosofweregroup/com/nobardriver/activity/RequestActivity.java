package nobar.vallcosofweregroup.com.nobardriver.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.adapter.RequestAdapter;
import nobar.vallcosofweregroup.com.nobardriver.global.AppPreferences;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;
import nobar.vallcosofweregroup.com.nobardriver.global.TemporaryPreferences;
import nobar.vallcosofweregroup.com.nobardriver.network.NetworkEventListener;
import nobar.vallcosofweregroup.com.nobardriver.network.OkHttpHandler;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructData;
import nobar.vallcosofweregroup.com.nobardriver.struct.StructRequest;

/**
 * Created by Javad Vatan on 10/23/2017.
 */

public class RequestActivity extends AppCompatActivity implements View.OnClickListener,
        RequestAdapter.RequestAdapterHandler {
    private static final int STATUS_CANCEL_ORDER = 0;
    private static final int STATUS_FIND_DESTINATION = 1;
    private static final int STATUS_ACCEPT_DETAIL_ORDER = 2;
    private static final int STATUS_WAIT_FOR_START_PROCESS = 3;
    private static final int STATUS_IN_CARRAY_PROCESS = 5;
    private static final int STATUS_SHOW_FACTOR = 6;
    private RecyclerView mRecyclerView;
    private RequestAdapter mAdapter;
    private List<StructRequest> dataList = new ArrayList<>();
    private StructData structData;
    private FrameLayout flConfirm;
    private TextView tvConfirm;
    private ProgressBar pbConfirm;
    private int driverStatus;
    private TemporaryPreferences tempPref;
    private String remainTime;

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        initVariable();
        initHeader();
        setOnClick();
        initList();

        //getData();
    }

    private void initVariable() {
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());
        tempPref = TemporaryPreferences.getInstance();
        structData = tempPref.getPrefData();
        mRecyclerView = findViewById(R.id.activity_request_recycler);
        flConfirm = findViewById(R.id.activity_request_fl_confirm);
        tvConfirm = findViewById(R.id.activity_request_tv_confirm);
        pbConfirm = findViewById(R.id.activity_request_pb_confirm);
    }

    @SuppressLint("SetTextI18n")
    private void initHeader() {
        ((TextView) findViewById(R.id.include_app_bar_tv_title))
                .setText("#" + structData.getId());
     /*   ImageView ivBack = findViewById(R.id.include_app_bar_iv_back);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(this);*/
    }

    private void setOnClick() {
        int[] viewId = new int[]{R.id.include_app_bar_iv_delete};
        for (int aViewId : viewId) {
            View view = findViewById(aViewId);
            view.setOnClickListener(this);
            view.setVisibility(View.VISIBLE);
        }
        flConfirm.setOnClickListener(this);
    }

    private void initList() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new RequestAdapter(this, dataList);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void manageStatus() {
        switch (tempPref.getPrefInt("driverStatus", STATUS_FIND_DESTINATION)) {
            case STATUS_FIND_DESTINATION:
                setStatus1();
                break;

            case STATUS_ACCEPT_DETAIL_ORDER:
                //   setStatus2();
                break;

            case STATUS_WAIT_FOR_START_PROCESS:
                setStatus3();
                break;

            case STATUS_IN_CARRAY_PROCESS:
                setStatus4();
                break;

            case STATUS_SHOW_FACTOR:
                setStatus5();
                break;
        }
    }
/*

    private void callApiCancelOrder() {

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {

                    GlobalFunction.getInstance().toast(RequestActivity.this,
                            object.getString(Constants.JSON_MESSAGE));
                    manageCancelOrder();
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                GlobalFunction.getInstance().toast(RequestActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_CANCEL_ORDER, eventListener)
                .addParam(Constants.JSON_ORDER_ID, structData.getId())
                .send();
    }
*/

    private void manageCancelOrder() {
        TemporaryPreferences.getInstance().clearPreferences();
        Intent intent = new Intent(RequestActivity.this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void setStatus1() {
        dataList.clear();
        flConfirm.setVisibility(View.VISIBLE);
        tvConfirm.setText(R.string.recive_into_destination);
        driverStatus = STATUS_FIND_DESTINATION;
        tempPref.addPrefInt("driverStatus", STATUS_FIND_DESTINATION);
        String title = "تایید سفارش";
        String detail = "کد سفارش : " + structData.getId();
        dataList.add(new StructRequest(title, detail, R.drawable.ic_down, R.drawable.bg_circle_white,
                R.drawable.ic_perview, true));

        dataList.add(new StructRequest("رسیدن به مبدا ", null,
                R.drawable.ic_cat_location, R.drawable.bg_circle_blue,
                R.drawable.ic_location, true));
        mAdapter.notifyDataSetChanged();
    }

    private void setStatus2() {
        setStatus1();
        tvConfirm.setText(R.string.accept_detail_order);
        driverStatus = STATUS_ACCEPT_DETAIL_ORDER;
        tempPref.addPrefInt("driverStatus", STATUS_ACCEPT_DETAIL_ORDER);

        dataList.set(1, new StructRequest("رسیدن به مبدا ", null,
                R.drawable.ic_down, R.drawable.bg_circle_white,
                R.drawable.ic_location, true));

        dataList.add(new StructRequest(getString(R.string.step_tree_title),
                getString(R.string.step_tree_detail),
                R.drawable.ic_cat_detail_order, R.drawable.bg_circle_blue,
                R.drawable.ic_pen, true));
    }

    private void setStatus3() {
        setStatus1();
        //setStatus2();
        tvConfirm.setText(R.string.start_process);
        driverStatus = STATUS_WAIT_FOR_START_PROCESS;
        tempPref.addPrefInt("driverStatus", STATUS_WAIT_FOR_START_PROCESS);

        dataList.set(1, new StructRequest("رسیدن به مبدا ", null,
                R.drawable.ic_down, R.drawable.bg_circle_white,
                R.drawable.ic_location, true));
        /*
        dataList.set(2, new StructRequest(getString(R.string.step_tree_title),
                getString(R.string.step_tree_detail),
                R.drawable.ic_cat_pen, R.drawable.bg_circle_white,
                0*//*R.drawable.ic_pen*//*, false));*/

        dataList.add(new StructRequest(getString(R.string.step_four_title),
                getString(R.string.step_four_detail),
                R.drawable.ic_cat_car, R.drawable.bg_circle_blue,
                0));

        mAdapter.notifyDataSetChanged();
    }

    private void setStatus4() {
        setStatus1();
        // setStatus2();
        setStatus3();
        tvConfirm.setVisibility(View.VISIBLE);
        tvConfirm.setText(R.string.end_process);
        driverStatus = STATUS_IN_CARRAY_PROCESS;
        tempPref.addPrefInt("driverStatus", STATUS_IN_CARRAY_PROCESS);


      /*  String title = getString(R.string.title_step_3);
        String detail = getString(R.string.detail_step3);
        dataList.set(2, new StructRequest(title, detail, R.drawable.ic_pen, R.drawable.bg_circle_white,
                R.drawable.ic_perview));*/

        dataList.set(2, new StructRequest(getString(R.string.step_four_third_title),
                remainTime,
                R.drawable.ic_cat_car, R.drawable.bg_circle_blue, true,
                0));


        mAdapter.notifyDataSetChanged();
    }

    private void setStatus5() {
        setStatus1();
        // setStatus2();
        setStatus3();
        tvConfirm.setVisibility(View.VISIBLE);
        tvConfirm.setText("مشاهده صورت حساب");
        flConfirm.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.bottom_navigation_color), PorterDuff.Mode.MULTIPLY);
        driverStatus = STATUS_SHOW_FACTOR;
        tempPref.addPrefInt("driverStatus", STATUS_SHOW_FACTOR);
        String timeFormat = "";
        if (remainTime != null) {
            int remainIntTime = Math.abs(Integer.parseInt(remainTime));
            timeFormat = String.format("%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(remainIntTime * 1000),
                    TimeUnit.MILLISECONDS.toMinutes(remainIntTime * 1000) % 60,
                    TimeUnit.MILLISECONDS.toSeconds(remainIntTime * 1000) % 60);
        }
        dataList.set(2, new StructRequest(getString(R.string.step_four_fourth_title),
                timeFormat,
                R.drawable.ic_down, R.drawable.bg_circle_white, false,
                0));


        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClicked(int position) {
        switch (position) {
            case 0:
                manageFactor(FactorActivity.TAG_MODE_REVIEW);
                break;

            case 1:
                manageShowPathOnMap();
                break;

            case 2:
                startActivity(new Intent(this, DetailOrderActivity.class));
                break;
        }
    }

    private void manageShowPathOnMap() {
        Intent mapIntent = new Intent(this, MapActivity.class);
        LatLng tehran = new LatLng(35.6892, 51.3890);
        mapIntent.putExtra(MapActivity.TAG_MAP_LAT, tehran.latitude);
        mapIntent.putExtra(MapActivity.TAG_MAP_LNG, tehran.longitude);
        startActivity(mapIntent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.include_app_bar_iv_back:
                onBackPressed();
                break;

            case R.id.include_app_bar_iv_delete:
                driverStatus = STATUS_CANCEL_ORDER;
                callApiUpdateOrderDriverStatus(Constants.STATUS_DRIVER_CANCEL_ORDER);
                break;

            case R.id.activity_request_fl_confirm:
                manageConfirm();
                break;
        }
    }

    private void manageConfirm() {
        switch (driverStatus) {

            case STATUS_FIND_DESTINATION:
                setStatus3();
                break;

            case STATUS_ACCEPT_DETAIL_ORDER:
                callApiUpdateOrderDriverStatus(Constants.STATUS_DRIVER_ACCEPT_DETAIL_ORDER);
                break;

            case STATUS_WAIT_FOR_START_PROCESS:
                callApiUpdateOrderDriverStatus(Constants.STATUS_DRIVER_START_PROCESS);
                break;

            case STATUS_IN_CARRAY_PROCESS:
                callApiUpdateOrderDriverStatus(Constants.STATUS_DRIVER_END_PROCESS);
                break;

            case STATUS_SHOW_FACTOR:
                manageFactor(FactorActivity.TAG_MODE_FINAL);
                break;
        }
    }

    private void callApiUpdateOrderDriverStatus(String statusNumber) {
        tvConfirm.setVisibility(View.INVISIBLE);
        pbConfirm.setVisibility(View.VISIBLE);
        NetworkEventListener networkEventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200) ||
                        object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_201)) {
                    tvConfirm.setVisibility(View.VISIBLE);
                    pbConfirm.setVisibility(View.INVISIBLE);
                    manageSuccessUpdateOrderStatus();
                    GlobalFunction.getInstance().toast(RequestActivity.this,
                            object.getString(Constants.JSON_MESSAGE));
                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            @Override
            public void onError(String string) {
                tvConfirm.setVisibility(View.VISIBLE);
                pbConfirm.setVisibility(View.INVISIBLE);
                GlobalFunction.getInstance().toast(RequestActivity.this, string);
            }
        };

        new OkHttpHandler(this, Constants.API_UPDATE_ORDER_DRIVER_STATUS, networkEventListener)
                .addParam(Constants.JSON_ORDER_ID, structData.getId())
                .addParam(Constants.JSON_DRIVER_ID, AppPreferences.getInstance().getUserId())
                .addParam(Constants.JSON_STATUS, statusNumber)
                .send();
    }

    private void manageSuccessUpdateOrderStatus() {
        switch (driverStatus) {

            case STATUS_CANCEL_ORDER:
                manageCancelOrder();
                break;

            case STATUS_ACCEPT_DETAIL_ORDER:
                setStatus3();
                break;

            case STATUS_WAIT_FOR_START_PROCESS:
                setStatus4();
                break;

            case STATUS_IN_CARRAY_PROCESS:
                setStatus5();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        callApiGetOrderStatus();
    }

    private void callApiGetOrderStatus() {

        NetworkEventListener eventListener = new NetworkEventListener() {
            @Override
            public void onSuccess(JSONObject object) throws JSONException {
                if (object.getString(Constants.JSON_STATUS).equals(Constants.JSON_STATUS_200)) {

                    JSONObject jsonObject = object.getJSONObject(Constants.JSON_DATA);

                    if (jsonObject.has(Constants.JSON_TIME_CAL))
                        remainTime = jsonObject.getString(Constants.JSON_TIME_CAL);

                    manageStatus();

                } else {
                    onError(object.getString(Constants.JSON_MESSAGE));
                }
            }

            public void onError(String string) {
                if (string != null && string.length() > 0 && !string.equals(getString(R.string.spam_message_from_server)))
                    GlobalFunction.getInstance().toast(RequestActivity.this, string);
                manageStatus();
            }
        };

        String url = Constants.API_GET_ORDER_STATUS + structData.getId();

        new OkHttpHandler(this, url, eventListener)
                .send();
    }

    private void manageFactor(int type) {
        Intent intent = new Intent(this, FactorActivity.class);
        intent.putExtra(FactorActivity.TAG_MODE, type);
        startActivity(intent);
    }
}
