package nobar.vallcosofweregroup.com.nobardriver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import nobar.vallcosofweregroup.com.nobardriver.R;
import nobar.vallcosofweregroup.com.nobardriver.global.AppPreferences;
import nobar.vallcosofweregroup.com.nobardriver.global.Constants;
import nobar.vallcosofweregroup.com.nobardriver.global.GlobalFunction;
import nobar.vallcosofweregroup.com.nobardriver.global.TemporaryPreferences;
import nobar.vallcosofweregroup.com.nobardriver.main.FragmentPagerAdapter;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    private ViewPager vViewPager;
    private ImageView ivStar, ivSettlement, ivDoing, ivHistory, ivHome;
    private TextView tvPageTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize() {
        manageActiveOrder();
        GlobalFunction.getInstance().overrideFonts(this, getWindow().getDecorView().getRootView());

        vViewPager = findViewById(R.id.main_view_pager);
        ivStar = findViewById(R.id.main_iv_star);
        ivSettlement = findViewById(R.id.main_iv_settlement);
        ivDoing = findViewById(R.id.main_iv_doing);
        ivHistory = findViewById(R.id.main_iv_history);
        ivHome = findViewById(R.id.main_iv_home);
        tvPageTitle = findViewById(R.id.include_app_bar_tv_title);


        ivStar.setOnClickListener(this);
        ivSettlement.setOnClickListener(this);
        ivHome.setOnClickListener(this);
        ivHistory.setOnClickListener(this);
        ivDoing.setOnClickListener(this);
        findViewById(R.id.include_app_bar_iv_profile).setOnClickListener(this);
        findViewById(R.id.include_app_bar_iv_logout).setOnClickListener(this);
        findViewById(R.id.include_app_bar_iv_profile).setVisibility(View.VISIBLE);
        findViewById(R.id.include_app_bar_iv_logout).setVisibility(View.VISIBLE);

        initViewPager();
    }

    private void manageActiveOrder() {
        if (TemporaryPreferences.getInstance()
                .getPrefBool(TemporaryPreferences.PREF_IS_ACTIVE_ORDER, false))
            startActivity(new Intent(this, RequestActivity.class));
    }

    private void initViewPager() {
        FragmentPagerAdapter adapter = new FragmentPagerAdapter(this, 4, FragmentPagerAdapter.PAGE_MAIN);

        vViewPager.setAdapter(adapter);
        vViewPager.setCurrentItem(Constants.TAB_HOME);
        selectTab(R.id.main_iv_home);
        vViewPager.setOffscreenPageLimit(3);
    }

    private void selectTab(int id) {
        switch (vViewPager.getCurrentItem()) {
            case Constants.TAB_STAR:
                ivStar.setImageResource(R.drawable.tab_ic_star);
                break;

            case Constants.TAB_SETTLEMENT:
                ivSettlement.setImageResource(R.drawable.tab_ic_settlement);
                break;

         /*   case Constants.TAB_DOING:
                ivDoing.setImageResource(R.drawable.tab_ic_car);
                break;*/

            case Constants.TAB_HISTORY:
                ivHistory.setImageResource(R.drawable.tab_ic_history);
                break;

            case Constants.TAB_HOME:
                ivHome.setImageResource(R.drawable.tab_ic_home);
                break;
        }

        switch (id) {
            case R.id.main_iv_star:
                ivStar.setImageResource(R.drawable.tab_ic_star_fill);
                vViewPager.setCurrentItem(Constants.TAB_STAR);
                tvPageTitle.setText(R.string.page_title_rate);


                break;

            case R.id.main_iv_settlement:
                ivSettlement.setImageResource(R.drawable.tab_ic_settlement_fill);
                vViewPager.setCurrentItem(Constants.TAB_SETTLEMENT);
                tvPageTitle.setText(R.string.patge_title_settlement);

                break;

           /* case R.id.main_iv_doing:
                ivDoing.setImageResource(R.drawable.tab_ic_car);
                vViewPager.setCurrentItem(Constants.TAB_DOING);
                break;*/

            case R.id.main_iv_history:
                ivHistory.setImageResource(R.drawable.tab_ic_history_fill);
                vViewPager.setCurrentItem(Constants.TAB_HISTORY);
                tvPageTitle.setText(R.string.page_title_history);

                break;

            case R.id.main_iv_home:
                ivHome.setImageResource(R.drawable.tab_ic_home_fill);
                vViewPager.setCurrentItem(Constants.TAB_HOME);
                tvPageTitle.setText(R.string.page_title_home);
                break;
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.main_iv_star:
            case R.id.main_iv_settlement:
            case R.id.main_iv_history:
            case R.id.main_iv_home:
                selectTab(view.getId());
                break;
            case R.id.include_app_bar_iv_profile:
                startActivity(new Intent(this, ProfileActivity.class));
                break;

            case R.id.main_iv_doing:
                manageDriverStatus();
                break;

            case R.id.include_app_bar_iv_logout:
                manageLogout();
                break;
        }
    }

    private void manageDriverStatus() {
        AppPreferences preferences = AppPreferences.getInstance();
        if (preferences.isActiveDriver()) {
            ivDoing.setImageResource(R.drawable.tab_ic_off);
            preferences.setActiveDriver(false);
        } else {
            ivDoing.setImageResource(R.drawable.tab_ic_car);
            preferences.setActiveDriver(true);
        }

    }

    private void manageLogout() {
        AppPreferences.getInstance().clearPreferences();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void callApiGetLastDriverStatus() {

    }

    private void callApiUpdateDriverStatus() {

    }
}
